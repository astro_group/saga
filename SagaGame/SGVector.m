//
//  SGVector.m
//  SagaGame
//
//  Created by Lucas Araujo on 4/8/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import "SGVector.h"

@implementation SGVector

//Soma dois vetores
+ (CGPoint) sumVector: (CGPoint)vector1 and: (CGPoint)vector2{
    
    CGPoint result;
    result = CGPointMake(vector1.x + vector2.x,
                         vector1.y + vector2.y);
    return result;
}

//Multiplica vetor por escalar
+ (CGPoint) multiplyVector: (CGPoint)vector withScalar: (CGFloat) scalar{
    
    CGPoint result;
    result = CGPointMake(vector.x *scalar,
                         vector.y*scalar);
    
    return result;
}

//Inverte todas as componentes do vetor
+ (CGPoint) invertVector:(CGPoint)vector{
    
    CGPoint result;
    result = CGPointMake(-vector.x, -vector.y);
    
    return result;
}

//Calcula modulo do vetor
+ (CGFloat) module:(CGPoint)vector{
    
    CGFloat result;
    result = sqrtf(vector.x*vector.x + vector.y*vector.y);
    
    return  result;
}

//Calcula direçao do vetor
+ (CGPoint) unityVector:(CGPoint)vector{
    
    CGPoint result;
    result = CGPointMake(vector.x/[ self module:vector], vector.y/[ self module:vector]);
    
    return result;
}

//Inverter componente x do vetor
+ (CGPoint) invertXFromVector:(CGPoint)vector{
    
    CGPoint result;
    result = CGPointMake(-vector.x, vector.y);
    
    return result;
}

+(CGPoint) rotateVector:(CGPoint)vector WithCenter:(CGPoint)center FromAngle:(CGFloat)angle{
    
    CGPoint result = CGPointZero;
    
    if (center.x == 0 && center.y ==0) {
        result.x = vector.x*cosf(angle) - vector.y*sinf(angle);
        result.y = vector.x*sinf(angle) + vector.y*cosf(angle);
        
    } else {
       //Coloca o centro na origem
        vector.x -= center.x;
        vector.y -= center.y;
        
        //Calcula o resultante
        result.x = vector.x*cosf(angle) - vector.y*sinf(angle);
        result.y = vector.x*sinf(angle) + vector.y*cosf(angle);
        
        //Retorna o resultante para o lugar certo
        result.x += center.x;
        result.y += center.y;
    }
    
    return result;
}

@end
