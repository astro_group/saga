//
//  GameKitHelper.m
//  SagaGame
//
//  Created by Lucas Araujo on 4/22/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import "GameKitHelper.h"

NSString *const PresentAuthenticationViewController = @"present_authentication_view_controller";

@implementation GameKitHelper{
    BOOL _enableGameCenter;
}

+ (instancetype)sharedGameKitHelper {
    
    static GameKitHelper *sharedGameKitHelper;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedGameKitHelper = [[GameKitHelper alloc] init];
    });
    return sharedGameKitHelper;
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _enableGameCenter = YES;
    }
    return self;
}

- (void) reportScore:(int64_t)score forLeaderBoardID:(NSString *)leaderboardID{
    if (!_enableGameCenter) {
        NSLog(@"Local play is not authenticated");
    }
    //Guardar informaçoes do score
    GKScore *scoreReporter = [[GKScore alloc]
                              initWithLeaderboardIdentifier:leaderboardID]; scoreReporter.value = score;
    
    scoreReporter.context = 0;
    NSArray *scores = @[scoreReporter];
    
    //Mandar score
    [GKScore reportScores:scores withCompletionHandler:^(NSError *error) {
        [self setLastError:error]; }];
}

//Guarda erros recebidos
- (void)setLastError:(NSError *)error
{
    //Guarda último erro
    _lastError = [error copy];
    if (_lastError) {
        NSLog(@"GameKitHelper ERROR: %@", [[_lastError userInfo] description]);
    }
}


- (void)setAuthenticationViewController: (UIViewController *)authenticationViewController
{
    //Notificaçao para quando authenticationViewController estiver presente
    if (authenticationViewController != nil)
    {
        //Guarda a viewController
        _authenticationViewController = authenticationViewController;
        
        //Mandar notificaçao
        [[NSNotificationCenter defaultCenter] postNotificationName:PresentAuthenticationViewController object:self];
    }
}

- (void) authenticateLocalPlayer{
    
    // Receber player local
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    // Controlador de autenticaçao
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error) {
        
        //Guarda erros recebidos
        [self setLastError:error];
        if(viewController != nil) {
            
            //Definir viewController de autenticaçao
            [self setAuthenticationViewController:viewController];
        }
        
        //Caso o jogador já esteja autenticado
        else if([GKLocalPlayer localPlayer].isAuthenticated) {
            
                //Definir game center para SIM
                _enableGameCenter = YES;
        }
        //Caso o jogador nāo esteja logado ou tenha apertado no butao de cancel ou login falhou
        else {
                //Definir game center para NAO
                _enableGameCenter = NO;
        }
    };
}

@end
