//
//  MyScene.m
//  SagaGame
//
//  Created by Naiara Moura on 13/04/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import "MyScene.h"
#import "GameScene.h"
#import "Block.h"
#import "SGVector.h"
#import "Text.h"


@implementation MyScene

//CENA INICIAL DO JOGO


//Acrescentando o backgraund e o botão de start
-(id)initWithSize:(CGSize)size{

    
    if(self = [super initWithSize:size]){
        
    //Inicializar música
        NSURL *urlMusic = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/GameMusic222.wav", [[NSBundle mainBundle] resourcePath]]];
        NSError *error;
        
        self.titleTheme = [[AVAudioPlayer alloc] initWithContentsOfURL:urlMusic error:&error];
        self.titleTheme.numberOfLoops = -1;
        
        if (!self.titleTheme)
            NSLog(@"%@",[error localizedDescription]);
        else
            [self.titleTheme play];

        
    //Backgraund
        self.backgroundColor = [SKColor whiteColor];
        
        CGRect imageSize = CGRectMake(0.0, 0.0, 802.0, 320.0);
        
        //Colocando o background na terceira layer.
        SKSpriteNode *background = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"backgraundInicial2.png"] size:imageSize.size];
        background.anchorPoint = CGPointZero;
        background.scale = self.size.height/background.size.height;
        [self addChild:background];        
        
    //Logomarca na tela
        SKTexture *logoTexture = [SKTexture textureWithImageNamed:@"Logo324x180"];
        logoTexture.filteringMode = SKTextureFilteringNearest;
        
        SKSpriteNode *logo = [SKSpriteNode spriteNodeWithTexture:logoTexture];
        logo.position = CGPointMake(self.size.width / 2.0, self.size.height-150);
        logo.scale = 1.2;
        logo.name = @"logo";
        [self addChild:logo];
        
    //Botao de inicio
        Text *buttonStart = [Text labelNodeWithFontNamed:@"Thirteen Pixel Fonts"];
        buttonStart.position = CGPointMake(self.size.width / 2.0, self.size.height/6);
        buttonStart.fontSize = 20.0;
        buttonStart.text = @"START";
        buttonStart.name = @"buttonStart";
        buttonStart.fontColor = [SKColor whiteColor];
        [self addChild:buttonStart];

 
    //botao de ir para tela de help
        SKLabelNode *buttonHelp = [SKLabelNode labelNodeWithFontNamed:@"Thirteen Pixel Fonts"];
        buttonHelp.position = CGPointMake(self.size.width / 2.0, self.size.height-240);
        buttonHelp.fontSize = 20.0;
        buttonHelp.text = @"HELP";
        buttonHelp.name = @"buttonHelp";
        buttonHelp.fontColor = [SKColor whiteColor];
        [self addChild:buttonHelp];
        
       
        
}
    return self;
}


//Ao apertar botao ir para as cenas
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
    SKNode *node = [self nodeAtPoint:location];
    SKTransition *transition = [SKTransition fadeWithColor:[UIColor colorWithRed:140.0/255.0
                                                                           green:171.1/255.0
                                                                            blue:186.0/255.0
                                                                           alpha:2]
                                                  duration:2.0];
    

    //Ao apertar botao de start ir para cena do jogo
    if ([node.name isEqualToString:@"buttonStart"] || [node.name isEqualToString:@"logo"]) {
                GameScene *game = [[GameScene alloc] initWithSize:CGSizeMake(self.size.width, self.size.height)];
        
        //Remover musica
        [self.titleTheme stop];
        
        //Mudar de Tela
        [self.scene.view presentScene:game transition:transition];
    }
    
    //Vai para cena de ajuda
    if ([node.name isEqualToString:@"buttonHelp"]) {
        HelpScene *helpScene = [[HelpScene alloc] initWithSize:CGSizeMake(self.size.width, self.size.height)];
        
        [self.scene.view presentScene:helpScene transition:transition];
    }
 
}


-(void)update:(NSTimeInterval)currentTime{
    
}
@end
