//
//  GameKitHelper.h
//  SagaGame
//
//  Created by Lucas Araujo on 4/22/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import <Foundation/Foundation.h>

@import GameKit;
extern NSString *const PresentAuthenticationViewController;

@interface GameKitHelper : NSObject

@property (nonatomic, readonly) UIViewController *authenticationViewController;
@property (nonatomic, readonly) NSError *lastError;

+ (instancetype)sharedGameKitHelper;

- (void)authenticateLocalPlayer;

- (void) reportScore:(int64_t) score forLeaderBoardID: (NSString *) leaderboardID;

@end
