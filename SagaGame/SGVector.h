//
//  SGVector.h
//  SagaGame
//
//  Created by Lucas Araujo on 4/8/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface SGVector : NSObject

+ (CGPoint) sumVector: (CGPoint)vector1 and: (CGPoint)vector2;

+ (CGPoint) multiplyVector: (CGPoint)vector withScalar: (CGFloat) scalar;

+ (CGPoint) invertVector:(CGPoint) vector;

+ (CGFloat) module:(CGPoint) vector;

+ (CGPoint) unityVector:(CGPoint) vector;

+ (CGPoint) invertXFromVector:(CGPoint) vector;

+(CGPoint) rotateVector:(CGPoint)vector WithCenter:(CGPoint)center FromAngle:(CGFloat)angle;
@end
