//
//  GameOverScene.h
//  SagaGame
//
//  Created by Naiara Moura on 14/04/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GameScene.h"


@interface GameOverScene : SKScene

@property (nonatomic) int valorRecebidoScore;
@property (nonatomic) BOOL hasSentScore;

@end
