//
//  Text.m
//  SagaGame
//
//  Created by Lucas Pereira on 4/27/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import "Text.h"

@implementation Text

-(instancetype)initWithFontNamed:(NSString *)fontName{
    UIFont *f = [UIFont fontWithName:fontName size:20];
    
    self= [super initWithFontNamed:f.fontName];
    
    return self;
}

@end
