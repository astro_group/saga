//
//  MyScene.h
//  SagaGame
//
//  Created by Naiara Moura on 13/04/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <SpriteKit/SpriteKit.h>
#import "HelpScene.h"


@interface MyScene : SKScene

@property (nonatomic) AVAudioPlayer *titleTheme;

@end
