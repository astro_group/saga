//
//  UmbraNavigationController.m
//  SagaGame
//
//  Created by Lucas Araujo on 4/22/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import "UmbraNavigationController.h"
#import "GameKitHelper.h"

@interface UmbraNavigationController ()

@end

@implementation UmbraNavigationController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showAuthenticationViewController)
                                                 name:PresentAuthenticationViewController
                                               object:nil];
    
    [[GameKitHelper sharedGameKitHelper] authenticateLocalPlayer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAuthenticationViewController {
    
    
    //Autenticar o jogador logo ao iniciar o jogo
    GameKitHelper *gameKitHelper = [GameKitHelper sharedGameKitHelper];
    
    [self.topViewController presentViewController: gameKitHelper.authenticationViewController
                                         animated:YES
                                       completion:nil];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
