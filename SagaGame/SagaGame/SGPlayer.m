//
//  SGPlayer.m
//  SagaGame
//
//  Created by Lucas Araujo on 4/8/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import "SGPlayer.h"

@implementation SGPlayer

-(void)setHp:(int)hp{
    if (hp <= 0) {
        _hp = 0;
    } else {
        _hp = hp;
    }
}

- (void) invalidatePlayerRelativeToEnemy:(Direction) attackDirection withImpulse:(CGPoint) impulse{
    
    self.isInvalid = YES;
    self.isInvunerable = YES;
    
    //Iniciar animaçao de invalidez
    [self startHittedAnimation];
    
    if(self.hp){
        //Rodar som de sofrimento
        [self runAction:self.sufferSound];
    }
    else{
        [self runAction:self.dieSound];
    }
    SKAction *sufferAttack;
    
    sufferAttack = [SKAction runBlock:^{
        
        if(attackDirection == LEFT){
            self.velocity = CGPointMake(-impulse.x, impulse.y);

        }
        else{
            self.velocity = CGPointMake(impulse.x, impulse.y);
        }
        
    }];
    
    [self runAction:sufferAttack];
}

-(void)recoverFromInvalidationInFloorHight: (CGFloat)floor{
//    NSLog(@"\rFloor: %f   Player y: %f  isTrue: %d", floor, self.position.y, (self.position.y <= floor));
    
    if (self.position.y <= floor) {
        
        SKAction *recover;
        SKAction *invalidTime = [SKAction waitForDuration:0.3];
        SKAction *invunarableTime = [SKAction waitForDuration:1.5];
        SKAction *recoverVunerability;
        SKAction *sequenceRecoverFromInvalidation;
        
        recover = [SKAction runBlock:^{
            if(self.direction == LEFT){
                self.velocity = CGPointMake(-self.velocityXDefault, self.velocity.y);
            }
            else{
                self.velocity = CGPointMake(self.velocityXDefault, self.velocity.y);
            }
            self.isInvalid = NO;
            
            [self removeActionForKey:@"playerHitted"];
        }];
        
        recoverVunerability = [SKAction runBlock:^{
            self.isInvunerable = NO;
        }];
        
        sequenceRecoverFromInvalidation = [SKAction sequence:@[invalidTime ,recover, invunarableTime, recoverVunerability]];
        
        [self runAction:sequenceRecoverFromInvalidation];
    }
}

- (void) attack{
    
    //Reduzir a velocidade do player durante o ataque
    SKAction *reduceVelocity = [SKAction runBlock:^{
        if(self.direction == LEFT)
            self.velocity = CGPointMake(-self.velocityXReduced, self.velocity.y);
        else
            self.velocity = CGPointMake(self.velocityXReduced, self.velocity.y);
    }];
    
    //Tempo de animacao
    SKAction *wait = [SKAction waitForDuration:0.15];
    
    //Fechar caixa
    SKAction *closeBox = [SKAction runBlock:^{
        
        [self removeActionForKey:@"playerAttacking"];
        
        self.attackBox = CGRectZero;
        [self.visualAttackBox removeFromParent];
        self.visualAttackBox = nil;
        self.isAttacking = NO;
    }];
    
    //Recuperar velocidade inicial
    SKAction *recoverVelocity = [SKAction runBlock:^{
        if(self.direction == LEFT){
            self.velocity = CGPointMake(-self.velocityXDefault , self.velocity.y);
        }
        else{
            self.velocity = CGPointMake(self.velocityXDefault, self.velocity.y);
        }
        
    }];
    
    //Fechar sequencia de açoes
    SKAction *sequence = [SKAction sequence:@[reduceVelocity,wait,closeBox,recoverVelocity]];
    
    //Caso nao esteja atacando e nao esteja invalido
    if(self.isAttacking == NO && !self.isInvalid)
    {
        //inicia ataque
        self.isAttacking = YES;
        
        //inicia Animaçao
        [self startAttackingAnimation];
        
        //Criar Caixa
        CGRect attackBox = CGRectMake(self.position.x, self.position.y, 55, 40);
        
        //Variável de correçao da origem do ataque
        CGFloat correctionOrigin = 30.0;
        
        //Metade da Altura Atual do player
        CGPoint playerCurrentCenter = CGPointMake(self.position.x + self.size.width/2, self.position.y + self.size.height/2);
        
        if(self.direction == LEFT)
        {
            //Definir origem em frente ao personagem à metade da altura para esquerda
            attackBox.origin = CGPointMake( attackBox.origin.x - self.size.width/2 + correctionOrigin, playerCurrentCenter.y);
        }
        else{
            //Definir origem em frente ao personagem à metade da altura para direita
            attackBox.origin = CGPointMake(attackBox.origin.x + self.size.width/2 - correctionOrigin, playerCurrentCenter.y);
        }
        
        //Definindo caixa do ataque do personagem
        self.attackBox = attackBox;
        
        //Criar box visual
        self.visualAttackBox = [[SKSpriteNode alloc] initWithColor:[UIColor whiteColor] size:self.attackBox.size];
        
        self.visualAttackBox.position = attackBox.origin;
        [self.parent addChild:self.visualAttackBox];
        self.visualAttackBox.alpha = 0.0;
        
        //Ativando a sequencia de açoes em Player
        [self runAction:sequence];
    }
    

}

- (void) dash{
    SKAction *acelerate;
    SKAction *wait;
    SKAction *recoverVelocity;
    
    acelerate = [SKAction runBlock:^{
        if (self.direction == LEFT) {
            self.velocity = CGPointMake(-700, self.velocity.y);
        } else {
            self.velocity = CGPointMake(700, self.velocity.y);
        }
        
    }];
    
    wait = [SKAction waitForDuration:0.2];
    
    recoverVelocity = [SKAction runBlock:^{
        if(self.direction == LEFT){
            self.velocity = CGPointMake(-self.velocityXDefault , self.velocity.y);
        }
        else{
            self.velocity = CGPointMake(self.velocityXDefault, self.velocity.y);
        }
    }];
    
    SKAction *sequence = [SKAction sequence:@[acelerate, wait, recoverVelocity]];
    
    [self runAction:sequence];
}

- (void) jump{
    
    //Iniciar animaçao de pulo
    [self startJumpingAnimation];
    
    //Aplicar velocidade de pulo
    self.velocity = [SGVector sumVector:self.velocity and:self.impulse];
}

- (void) startWalkingAnimation{
    if(self.walkingAnimation){
        self.animationStatus = WALKING;
        [self runAction:[SKAction repeatActionForever:self.walkingAnimation] withKey:@"playerWalking"];
    }
    else{
        NSLog(@"walkging animation nil");
    }
}

- (void) startJumpingAnimation{
    if(self.jumpingAnimation){
        self.animationStatus = JUMPING;
        [self runAction:[SKAction repeatActionForever:self.jumpingAnimation] withKey:@"playerJumping"];
    }
    else{
        NSLog(@"jumping animation nil");
    }
}

- (void) startAttackingAnimation{
    if(self.attackingAnimation){
        [self runAction:self.attackingAnimation withKey:@"playerAttacking"];
        [self runAction:self.attackSound];
    }
    else{
        NSLog(@"attacking animation nil");
    }
}

- (void) startFallingAnimation{
    if(self.fallingAnimation){
        self.animationStatus = FALLING;
        [self runAction:[SKAction repeatActionForever:self.fallingAnimation] withKey:@"playerFalling"];
    }
    else{
        NSLog(@"falling animation nil");
    }
}

- (void) startHittedAnimation{
    if(self.hittedAnimation){
        [self runAction:[SKAction repeatActionForever:self.hittedAnimation] withKey:@"playerHitted"];
    }
    else{
        NSLog(@"hitted animation nil");
    }
}

//Atualizar a posiçao do VisualBoundingBox
- (void) updateVisualBoundingBoxCenter{
    
    CGPoint center = CGPointMake(self.position.x, self.position.y + self.size.height/2);
    self.visualBoundingBox.position = center;
    
}

//Carrega animaçao de andar
-(void)loadPlayerWalkingAnimations{
    //Andando
    int nWalkingTextures = 4;
    NSMutableArray *walkingTextures = [NSMutableArray arrayWithCapacity:nWalkingTextures];
    
    //Coloca as imagens no array
    for (int i = 1; i <= nWalkingTextures; i++) {
        NSString *textureName = [NSString stringWithFormat:@"PlayerAndando/walk%d",i];
        SKTexture *playerTexture = [SKTexture textureWithImageNamed:textureName];
        playerTexture.filteringMode = SKTextureFilteringNearest;
        [walkingTextures addObject:playerTexture];
    }
    
    self.walkingAnimation = [SKAction animateWithTextures:walkingTextures timePerFrame:0.14];
}

//Carrega a animação de atacar do player
-(void)loadPlayerAttackAnimation{
    //Atacando
    int nAttackingTextures = 8;
    NSMutableArray *attackingTexturesLeft = [NSMutableArray arrayWithCapacity:nAttackingTextures/2];
    NSMutableArray *attackingTexturesRight = [NSMutableArray arrayWithCapacity:nAttackingTextures/2];
    
    //Coloca as imagens do ataque com a mao esquerda no array
    for (int i = 1; i <= nAttackingTextures/2; i++) {
        NSString *textureName = [NSString stringWithFormat:@"PlayerAtacando/attack%d",i];
        SKTexture *playerTexture = [SKTexture textureWithImageNamed:textureName];
        playerTexture.filteringMode = SKTextureFilteringNearest;
        [attackingTexturesLeft addObject:playerTexture];
    }
    
    //Coloca as imagens do ataque com a mao direita no array
    for (int i = nAttackingTextures/2 +1; i <= nAttackingTextures; i++) {
        NSString *textureName = [NSString stringWithFormat:@"PlayerAtacando/attack%d",i];
        SKTexture *playerTexture = [SKTexture textureWithImageNamed:textureName];
        playerTexture.filteringMode = SKTextureFilteringNearest;
        [attackingTexturesRight addObject:playerTexture];
    }
    
    self.attackingAnimationLeft = [SKAction animateWithTextures:attackingTexturesLeft timePerFrame:0.1];
    self.attackingAnimationRight = [SKAction animateWithTextures:attackingTexturesRight timePerFrame:0.1];
    
    
    self.attackingAnimation = [SKAction runBlock:^{
        
        if (self.attackingHand == RIGHT) {
            [self runAction:self.attackingAnimationRight];
        } else {
            [self runAction:self.attackingAnimationLeft];
        }
        
        self.attackingHand = !self.attackingHand;
    }];
    
}

//Carrega animação do Player de sofrer dano.
-(void)loadPlayerHittedAnimation{
    //Atacando
    int nDAmagedTextures = 1;
    NSMutableArray *damagedTextures = [NSMutableArray arrayWithCapacity:nDAmagedTextures];
    
    //Coloca as imagens no array
    for (int i = 1; i <= nDAmagedTextures; i++) {
        NSString *textureName = [NSString stringWithFormat:@"PlayerAposSofrerAtaque/hit%d",i];
        SKTexture *playerTexture = [SKTexture textureWithImageNamed:textureName];
        playerTexture.filteringMode = SKTextureFilteringNearest;
        [damagedTextures addObject:playerTexture];
    }
    
    self.hittedAnimation = [SKAction animateWithTextures:damagedTextures timePerFrame:0.1];
}


//Carrega animaçao de pulo
-(void) loadPlayerJumpingAnimation{
    
    int nJumpingTextures = 4;
    NSMutableArray *JumpingTextures = [NSMutableArray arrayWithCapacity:nJumpingTextures];
    
    //Coloca as imagens no array
    for (int i = 1; i <= nJumpingTextures - 1; i++) {
        NSString *textureName = [NSString stringWithFormat:@"PlayerPulando/jumping%d",i];
        SKTexture *playerTexture = [SKTexture textureWithImageNamed:textureName];
        playerTexture.filteringMode = SKTextureFilteringNearest;
        [JumpingTextures addObject:playerTexture];
    }
    
    NSString *textureName = [NSString stringWithFormat:@"PlayerPulando/jumping4"];
    SKTexture *playerTexture = [SKTexture textureWithImageNamed:textureName];
    playerTexture.filteringMode = SKTextureFilteringNearest;
    
    SKAction *freeJump = [SKAction animateWithTextures:@[playerTexture] timePerFrame:1];
    SKAction *jumpImpulse = [SKAction animateWithTextures:JumpingTextures timePerFrame:0.05];
    
    SKAction *jumpSequence = [SKAction sequence:@[jumpImpulse,freeJump]];
    
    self.jumpingAnimation = jumpSequence;
}

//Carrega animaçao de caida
-(void) loadPlayerFallingAnimation{
    
    int nFallingTextures = 3;
    NSMutableArray *fallingTextures = [NSMutableArray arrayWithCapacity:nFallingTextures];
    
    //Coloca as imagens no array
    for (int i = 1; i <= nFallingTextures -1; i++) {
        NSString *textureName = [NSString stringWithFormat:@"PlayerPulando/falling%d",i];
        SKTexture *playerTexture = [SKTexture textureWithImageNamed:textureName];
        playerTexture.filteringMode = SKTextureFilteringNearest;
        [fallingTextures addObject:playerTexture];
    }
    
    NSString *textureName = [NSString stringWithFormat:@"PlayerPulando/falling3"];
    SKTexture *playerTexture = [SKTexture textureWithImageNamed:textureName];
    playerTexture.filteringMode = SKTextureFilteringNearest;
    
    SKAction *falling = [SKAction animateWithTextures:fallingTextures timePerFrame:0.05];
    SKAction *freeFalling = [SKAction animateWithTextures:@[playerTexture] timePerFrame:1];
    SKAction *fallingSequence = [SKAction sequence:@[falling, freeFalling]];
    
    self.fallingAnimation = fallingSequence;
}



@end
