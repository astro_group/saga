//
//  UmbraNavigationController.h
//  SagaGame
//
//  Created by Lucas Araujo on 4/22/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UmbraNavigationController : UINavigationController

@end
