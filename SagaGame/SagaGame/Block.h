//
//  Block.h
//  SagaGame
//
//  Created by Lucas Pereira on 4/24/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Block : SKSpriteNode

@property (nonatomic) float angularVelocity;

@end
