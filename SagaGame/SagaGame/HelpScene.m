//
//  HelpScene.m
//  SagaGame
//
//  Created by Naiara Moura on 17/04/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import "HelpScene.h"
#import "SGPlayer.h"

@implementation HelpScene{
    //Movendo a imagem
    SKAction *moveLeft;
    SKAction *moveRight;
    
    //Personagen
    SGPlayer *_playerWalking;
    SGPlayer *_playerJunping;

    
    //Swipe
    UISwipeGestureRecognizer *_recornizer;
    
    SKSpriteNode *bgSprite;
    SKSpriteNode *bgSprite2;
//    SKSpriteNode *bichimTeste;
    
    //Layer em que todas as imagens estarao
    SKNode *_layerHelp;
    
    //imagem atual
    int _currentImage;
    
    //numero de imagens
    int _numberOfImages;
}

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {

        _layerHelp = [[SKNode alloc] init];
        
        //Definindo imagem atual
        _currentImage = 1;
        _numberOfImages = 2;
        
        [self addChild:_layerHelp];
        
        self.backgroundColor = [SKColor whiteColor];
        
        
        //Definindo primeira imagem
        bgSprite = [SKSpriteNode spriteNodeWithImageNamed:@"Layer2"];
        bgSprite.anchorPoint = CGPointZero;
        bgSprite.position = CGPointZero;
        bgSprite.scale = (self.size.width)/bgSprite.size.width;
        
        [_layerHelp addChild:bgSprite];
        
//        bichimTeste =[SKSpriteNode spriteNodeWithImageNamed:@"PlayerParado/standing1.png"];
//        bichimTeste.anchorPoint = CGPointZero;
//        bichimTeste.position = CGPointZero;
//        [bgSprite addChild:bichimTeste];
        
        
        //Definindo segunda imagem
        bgSprite2 = [SKSpriteNode spriteNodeWithImageNamed:@"Layer1"];
        bgSprite2.anchorPoint = CGPointZero;
        bgSprite2.position = CGPointMake(bgSprite.size.width, 0.0);
        bgSprite2.scale = (self.size.width)/bgSprite2.size.width;
        
        [_layerHelp addChild:bgSprite2];
        
        
        //Personagem andando
        CGRect rectOfPlayer = CGRectMake(0, 0, 236.0, 224.0);
        _playerWalking = [SGPlayer spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"PlayerAndando/walk1"] size:rectOfPlayer.size];
        _playerWalking.anchorPoint = CGPointZero;
        _playerWalking.position =CGPointMake(bgSprite.size.width, 0.0);
      
        [bgSprite addChild:_playerWalking];
        
        //Personagem pulando
        _playerJunping = [SGPlayer spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"PlayerAndando/walk1"] size:rectOfPlayer.size];
        _playerJunping.anchorPoint = CGPointZero;
        _playerJunping.position =CGPointMake(10, 0.0);
        
        [bgSprite addChild:_playerJunping];
        
        [self loadPlayerAnimations];
        [_playerWalking startWalkingAnimation];
        [_playerJunping jump];
//        [_playerJunping startJumpingAnimation];
        
        
        }
    return self;
}
-(void)didMoveToView:(SKView *)view{
    
   _recornizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    _recornizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [[self view] addGestureRecognizer:_recornizer];
    
    _recornizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    _recornizer.direction = UISwipeGestureRecognizerDirectionRight;
    [[self view] addGestureRecognizer:_recornizer];
   
}

-(void)loadPlayerAnimations{
    
    //Carregar animaçao de andar
    [_playerWalking loadPlayerWalkingAnimations];
    [_playerJunping loadPlayerJumpingAnimation];

}

// Passar tela
- (void)handleSwipe: (UISwipeGestureRecognizer *)sender{
  

    //Passar tela para a imagem da direita
    if( sender.direction == UISwipeGestureRecognizerDirectionLeft){
        if (_currentImage < _numberOfImages) {

        moveLeft = [SKAction moveByX: -self.size.width y:0 duration:0.5];
        [_layerHelp runAction:moveLeft];
            
            _currentImage ++;
        }
}
    
    //Passar tela para imagem da esquerda
    if (sender.direction == UISwipeGestureRecognizerDirectionRight) {
        if (_currentImage > 1) {
            
        moveRight = [SKAction moveByX: self.size.width y:0 duration:0.5];
        [_layerHelp runAction:moveRight];
            
            _currentImage --;
        }
  }
}

@end
