//
//  GlobalVars.h
//  SagaGame
//
//  Created by Lucas Araujo on 4/8/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#ifndef SagaGame_GlobalVars_h
#define SagaGame_GlobalVars_h

//Definindo direçao que algo se orienta
typedef enum {
    LEFT, //LEFT = 0
    RIGHT //RIGTH = 1
}Direction;

#endif
