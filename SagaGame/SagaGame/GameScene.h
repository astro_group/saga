//
//  GameScene.h
//  SagaGame
//

//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <SpriteKit/SpriteKit.h>
#import "SGPlayer.h"
#import "SGEnemy.h"
#import "GlobalVars.h"
#import "SGVector.h"
#import "GameOverScene.h"
#import "Life.h"


@interface GameScene : SKScene

@property (nonatomic) AVAudioPlayer *gameMusic;



@end
