//
//  SGEnemy.h
//  SagaGame
//
//  Created by Lucas Pereira on 4/9/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GlobalVars.h"

@interface SGEnemy : SKSpriteNode

@property (nonatomic) CGPoint velocity;
@property (nonatomic) Direction direction;
@property (nonatomic) CGRect attackBox;
@property (nonatomic) unsigned int hp;
@property (nonatomic) int distanceBetweenEnemyPlayer;
@property (nonatomic) float enemyVisionArea;
@property (nonatomic) float enemyAttackArea;
@property (nonatomic) CGFloat angularVelocityBigEnemyAttack;

//Properties referentes a IA
@property (nonatomic) int timeAttack;
@property (nonatomic) int delayResponseIA;
@property (nonatomic) float velocityXDefault;
@property (nonatomic) float attackBoxWidth;
@property (nonatomic) float attackBoxHeigth;
@property (nonatomic) int damage;
@property (nonatomic) CGPoint damageImpulse;
@property (nonatomic) float score;
@property (nonatomic) SKSpriteNode *visualAttackBox;
@property (nonatomic) SKSpriteNode *visualBoundingBox;
@property (nonatomic) BOOL isInvalid;
@property (nonatomic) BOOL isBorning;
@property (nonatomic) BOOL isWalking;
@property (nonatomic) BOOL isStandng;
@property (nonatomic) BOOL isAlive;
@property (nonatomic) BOOL isAttacking;

//Actions de som
@property (nonatomic) SKAction *bornSound;
@property (nonatomic) SKAction *attackSound;
@property (nonatomic) SKAction *sufferSound;
@property (nonatomic) SKAction *walkSound;
@property (nonatomic) SKAction *dieSound;
@property (nonatomic) SKAction *standingSound;

- (void) enemyDeath;
- (BOOL) stateOfEnemyRelativeToPlayer: (CGPoint) playerPoint;
- (void) invalidateEnemyRelativoToPlayerDirection:(Direction) direction;
- (void) attack;
- (void) setAttackBoxOrigin:(CGPoint)origin;

//Animacoes do inimigo comum
- (void) loadEnemyAnimations;
- (void) animationEnemyWalk;
- (void) animationEnemyAttack;
- (void) animationEnemyDamaged;
- (void) animationEnemyDie;
- (void) animationEnemyBorn;
- (void) animationEnemyStanding;

//Animacoes do inimigo grande
- (void) loadBigEnemyAnimations;
- (void) animationBigEnemyWalk;
- (void) animationBigEnemyAttack;
- (void) animationBigEnemyDamaged;
- (void) animationBigEnemyDie;
- (void) animationBigEnemyStanding;
- (void) animationBigEnemyBorn;

//Particulas do inimigo comum
@property (nonatomic) SKEmitterNode *comumParticles;
@property (nonatomic) SKEmitterNode *damageParticles;
-(void)initDamageParticlesInPositon:(CGPoint)position;

//Define o centro do visualBoundingBox para o centro do inimigo
- (void) updateVisualBoundingBoxCenter;

@end
