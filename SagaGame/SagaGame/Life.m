//
//  Life.m
//  SagaGame
//
//  Created by Tiago Pereira on 4/17/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import "Life.h"

@implementation Life

-(void)loadAnimations{}
-(void)loadFadingAnimation{}

-(void)loadConstantAnimation{
    int numFrame = 4;
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrame];
    
    for (int i=1; i <= numFrame; i++) {
        NSString *texturePath = [NSString stringWithFormat:@"Flame/flame%d",i];
        
        SKTexture *texture = [SKTexture textureWithImageNamed:texturePath];
        texture.filteringMode = SKTextureFilteringNearest;
        
        [textures addObject:texture];
    }
    
    self.constantAnimation = [SKAction animateWithTextures:textures timePerFrame:0.1];
}

@end
