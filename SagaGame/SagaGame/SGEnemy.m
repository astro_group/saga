//
//  SGEnemy.m
//  SagaGame
//
//  Created by Lucas Pereira on 4/9/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import "SGEnemy.h"

@implementation SGEnemy{
    
    //Animacoes do inimigo comum
    SKAction *_enemyWalkingAnimation;
    SKAction *_enemyAttackingAnimation;
    SKAction *_enemyDamegedAnimation;
    SKAction *_enemyBorningAnimation;
    SKAction *_enemyDyingAnimation;
    SKAction *_enemyStandingAnimation;
    
    //Animacoes do inimigo grande
    SKAction *_bigEnemyWalkingAnimation;
    SKAction *_bigEnemyAttackingAnimation;
    SKAction *_waitBigenemyAttackAnimation;
    SKAction *_bigEnemyAttackingAnimationUp;
    SKAction *_bigEnemyAttackingAnimationDown;
    SKAction *_bigEnemyDamegedAnimation;
    SKAction *_bigEnemyBorningAnimation;
    SKAction *_bigEnemyDyingAnimation;
    SKAction *_bigEnemyStandingAnimation;
    
    //Particles
    
}

-(void) enemyDeath{
    SKAction *waitDeathTimeAnimation = [SKAction waitForDuration:0];
    SKAction *removeEnemy = [SKAction runBlock:^{
        [self.visualAttackBox removeFromParent];
        self.visualAttackBox = nil;
        [self.visualBoundingBox removeFromParent];
        self.visualBoundingBox = nil;
        [self removeFromParent];
    }];
    SKAction *enemyDeath = [SKAction sequence:@[waitDeathTimeAnimation,removeEnemy]];
    
    [self runAction:enemyDeath];

    //Rodar som de morte do inimigo
    [self.parent runAction:self.dieSound];
}

//Maquina de estados do inimigo normal (IA)
- (BOOL) stateOfEnemyRelativeToPlayer: (CGPoint) playerPoint{
    
    //Checar se o inimigo está valido
    if(self.isAttacking == NO && self.isAlive == YES && self.isInvalid == NO && self.isBorning == NO)
    {
        
        //Faz o inimigo comum andar ou ficar parado
        if ([self.name isEqualToString:@"enemy"]) {
            //Se estiver parado
            if (self.velocity.x == 0) {
                if (self.isStandng == NO) {
                    self.isStandng = YES;
                    self.isWalking = NO;
                    
                    //Remove a animacao de andar
                    [self removeActionForKey:@"walk"];
                    
                    //Remove o som de andando
                    [self removeActionForKey:@"enemyWalkingSound"];
                    
                    //Rodar som de parado
                    if(self.standingSound)
                        [self runAction:[SKAction repeatActionForever:self.standingSound ] withKey:@"enemyStandingSound"];
                    
                    //Faz a animacao de ficar parado acontecer
                    [self runAction:[SKAction repeatActionForever:_enemyStandingAnimation] withKey:@"stand"];
                }
                
                //Se estiver andando
            } else {
                if (self.isWalking == NO) {
                    self.isStandng = NO;
                    self.isWalking = YES;
                    
                    //Remove a animacao de ficar parado
                    [self removeActionForKey:@"stand"];
                    
                    //Remove o som de parado
                    [self removeActionForKey:@"enemyStandingSound"];
                    
                    //Rodar som de andar
                    if(self.walkSound)
                        [self runAction:[SKAction repeatActionForever:self.walkSound] withKey:@"enemyWalkingSound"];
                    
                    //Faz a animacao de andar acontecer
                    [self runAction:[SKAction repeatActionForever:_enemyWalkingAnimation] withKey:@"walk"];
                }
            }
        //Faz o inimigo Grande andar ou ficar parado ou andar
        } else {
            //Se estiver parado
            if (self.velocity.x == 0) {
                if (self.isStandng == NO) {
                    self.isStandng = YES;
                    self.isWalking = NO;
                    
                    //Remove a animacao de andar
                    [self removeActionForKey:@"bigEnemyWalk"];
                    
                    //Faz a animacao de ficar parado acontecer
                    [self runAction:[SKAction repeatActionForever:_bigEnemyStandingAnimation] withKey:@"bigEnemyStand"];
                }
                
                //Se estiver andando
            } else {
                if (self.isWalking == NO) {
                    self.isStandng = NO;
                    self.isWalking = YES;
                    
                    //Remove a animacao de ficar parado
                    [self removeActionForKey:@"bigEnemyStand"];
                    
                    //Faz a animacao de andar acontecer
                    [self runAction:[SKAction repeatActionForever:_bigEnemyWalkingAnimation] withKey:@"bigEnemyWalk"];
                }
            }
        }
        
        
        //Distancia entre inimigo e player
        self.distanceBetweenEnemyPlayer = self.position.x - playerPoint.x;
        
        //Caso o player esteja à esquerda do Inimigo
        if ( self.distanceBetweenEnemyPlayer <= _enemyVisionArea &&  self.distanceBetweenEnemyPlayer >= 0) {
            self.velocity = CGPointMake(-self.velocityXDefault, 0);
            self.direction = LEFT;
            
            //Caso o player esteja à direita do Inimigo
        } else if ( self.distanceBetweenEnemyPlayer >= -_enemyVisionArea &&  self.distanceBetweenEnemyPlayer < 0){
            self.velocity = CGPointMake(self.velocityXDefault, 0);
            self.direction = RIGHT;
            
            //Caso o player esteja muito longe do Inimigo
        } else {
            self.velocity = CGPointZero;
        }
        
        //Define a direção do sprite
        if (self.direction == RIGHT && self.xScale < 0) {
            self.xScale *= -1;
        } else if (self.direction == LEFT && self.xScale > 0){
            self.xScale *= -1;
        }
        
        //Distancia entre inimigo e Player.
        int distanceEnemyPlayerModule;
        
        //Tirar o sinal da distancia entre player e inimigo
        if(self.distanceBetweenEnemyPlayer < 0){
            distanceEnemyPlayerModule = self.distanceBetweenEnemyPlayer * -1;
        }
        else{
            distanceEnemyPlayerModule = self.distanceBetweenEnemyPlayer;
        }
        
        //Caso o player esteja dentro da área de ataque do inimigo
        if(self.distanceBetweenEnemyPlayer <= self.enemyAttackArea)
        {
            //Retorna Verdadeiro caso tente atacar
            return true;
        }
    }
    
    //Retorna Falso caso nao Ataque
    return false;
}

- (void) invalidateEnemyRelativoToPlayerDirection:(Direction) direction{
    
    self.isInvalid = YES;
    SKAction *sufferAttack;
    SKAction *recover;
    SKAction *invalidTime = [SKAction waitForDuration:1];
    SKAction *sequenceInvalidMovement;
    
    sufferAttack = [SKAction runBlock:^{
        
        //Se o inimigo nao for o big enemy ele vai ser lancado
        if (!([self.name isEqualToString:@"bigEnemy"])) {
            if(direction == LEFT){
                [self runAction:[SKAction moveByX:-30 y:0 duration:0.2]];
            }
            else
                [self runAction:[SKAction moveByX:30 y:0 duration:0.2]];
        }
        
    }];
    
    recover = [SKAction runBlock:^{
        self.isInvalid = NO;
    }];
    sequenceInvalidMovement = [SKAction sequence:@[sufferAttack,invalidTime,recover]];
    
    [self runAction:sequenceInvalidMovement];
}

- (void) attack{
    //Iniciar Combate
    //Checar se player esta em área de ataque
    if( self.distanceBetweenEnemyPlayer <= _enemyAttackArea &&  self.distanceBetweenEnemyPlayer >= -_enemyAttackArea){
        
        //Checar se o player continua na área de ataque
        if( self.distanceBetweenEnemyPlayer <= _enemyAttackArea &&  self.distanceBetweenEnemyPlayer >= -_enemyAttackArea){
            
            SKAction *createAttackBox;
            
            //Definir Bandeira attacking para sim
            self.isAttacking = YES;
            
            //Rodar som de ataque
            [self.parent runAction:self.attackSound withKey:@"enemyAttackSound"];
            
            //Faz a animacao do inimigo normal acontecer
            if ([self.name isEqualToString:@"enemy"]) {
                [self runAction:_enemyAttackingAnimation];
            //Faz a animacao do inimigo grande acontecer
            } else {
                [self removeActionForKey:@"bigEnemyWalk"];
                [self runAction:_bigEnemyAttackingAnimation];
            }
            
            //Se o inimigo for normal ataca imediatamente
            if ([self.name isEqualToString:@"enemy"]) {
                
                createAttackBox = [SKAction runBlock:^{
                    //Criar Caixa
                    CGRect attackBox = CGRectMake(self.position.x, self.position.y, self.attackBoxWidth, self.attackBoxHeigth);
                    
                    //Criando Caixa visual de Ataque
                    self.visualAttackBox = [[SKSpriteNode alloc] initWithColor:[UIColor redColor] size:attackBox.size];
                    
                    if(self.direction == LEFT)
                    {
                        //Definir origem em frente ao personagem à metade da altura para esquerda
                        self.visualAttackBox.position = CGPointMake( self.position.x -self.size.width/5, self.position.y + (self.size.height/2)*1.2);
                    }
                    else{
                        //Definir origem em frente ao personagem à metade da altura para direita
                        self.visualAttackBox.position = CGPointMake( self.position.x +self.size.width/5, self.position.y + (self.size.height/2)*1.2);
                    }
                    
                    [self.parent addChild:self.visualAttackBox];
                    self.visualAttackBox.alpha = 0;
                    
                    //NSLog(@"Ataque Criado em x:%f y:%f", self.attackBox.origin.x,self.attackBox.origin.y);
                }];
            }
            else{
                //Acao de attack do inimigo grande
                createAttackBox = [SKAction runBlock:^{
                    //Criar Caixa
                    CGRect attackBox = CGRectMake(self.position.x, self.position.y, self.attackBoxWidth, self.attackBoxHeigth);
                    
                    //Criando Caixa visual de Ataque
                    self.visualAttackBox = [[SKSpriteNode alloc] initWithColor:[UIColor redColor] size:attackBox.size];
                    
                    if(self.direction == LEFT)
                    {
                        //Definir origem do colizor de ataque
                        self.visualAttackBox.position = CGPointMake( self.position.x, self.position.y + self.size.height - self.size.height/4);
                    }
                    else{
                        //Definir origem do colizor de ataque
                        self.visualAttackBox.position = CGPointMake( self.position.x, self.position.y + self.size.height - self.size.height/4);
                    }
                    
                    [self.parent addChild:self.visualAttackBox];
                    self.visualAttackBox.alpha = 0;
                    
                    //NSLog(@"Ataque Criado em x:%f y:%f", self.attackBox.origin.x,self.attackBox.origin.y);
                }];
            }

            
            //Reduzir a velocidade do inimigo durante o ataque
            SKAction *reduceVelocity = [SKAction runBlock:^{
                self.velocity = CGPointMake(0.0, self.velocity.y);
            }];
            
            SKAction *waitForBeginMecanism;
            SKAction *wait;
            SKAction *waitToAttackAgain;
            
            //Se for o inimigo comum coloca o tempo ajustado a ele
            if ([self.name isEqualToString:@"enemy"]) {
                //Tempo de animacao ate aparecer o ataque
                waitForBeginMecanism = [SKAction waitForDuration:0.15];
                //Tempo de animacao
                wait = [SKAction waitForDuration:1.5];
            
            //Se for o inimigo grande
            } else {
                //Tempo de animacao ate aparecer o ataque
                waitForBeginMecanism = [SKAction waitForDuration:5*0.2];
                //Tempo de animacao
                wait = [SKAction waitForDuration:6*0.15];
                waitToAttackAgain = [SKAction waitForDuration:3];
            }
            
            //Tempo de vida da caixa de ataque
            SKAction *lifeTimeAttackBox = [SKAction waitForDuration:0.25];
            
            //Fechar caixa
            SKAction *closeBox = [SKAction runBlock:^{
                self.attackBox = CGRectZero;
                NSLog(@"Caixa fechada");
                
                if(self.visualAttackBox){
                    [self.visualAttackBox removeFromParent];
                    self.visualAttackBox = nil;
                }
            }];
            
            SKAction *recoverVelocity = [SKAction runBlock:^{
                if(self.direction == LEFT){
                    self.velocity = CGPointMake(-100, self.velocity.y);
                }
                else{
                    self.velocity = CGPointMake(100, self.velocity.y);
                }
                
                //Recuperar o movimento e redefinir status
                self.isAttacking = NO;
            }];
            
            //Acao de esperar do inimigo
            SKAction *standingAction = [SKAction runBlock:^{
                
                //Se for o inimigo comum
                if ([self.name isEqualToString:@"enemy"]) {
                    //Remove o som de andando
                    [self removeActionForKey:@"enemyWalkingSound"];
                    
                    //Rodar som de parado
                    if(self.standingSound)
                        [self runAction:[SKAction repeatActionForever:self.standingSound ] withKey:@"enemyStandingSound"];
                    
                    [self runAction:[SKAction repeatActionForever:_enemyStandingAnimation] withKey:@"stand"];
                //Se for o inimigo grande
                } else {
                    [self runAction:[SKAction repeatActionForever:_bigEnemyStandingAnimation] withKey:@"bigEnemystand"];
                }
                
            }];
            
            //Fechar sequencia de açoes
            SKAction *sequence;
            //Se for o inimigo normal
            if ([self.name isEqualToString:@"enemy"]) {
                sequence = [SKAction sequence:@[reduceVelocity,waitForBeginMecanism,createAttackBox,lifeTimeAttackBox,closeBox,standingAction,wait,recoverVelocity]];
            //Se for o inimigo grande
            } else {
                sequence = [SKAction sequence:@[reduceVelocity,waitForBeginMecanism,createAttackBox,wait,standingAction,waitToAttackAgain,closeBox,recoverVelocity]];
            }
            
            //Ativando a sequencia de açoes em Player
            [self runAction:sequence];
        }
    }

}

- (void)setAttackBoxOrigin:(CGPoint)origin{
    
    _attackBox.origin = origin;
    _visualAttackBox.position = origin;
}

//Carrega todas as animacoes do inimigo comum
-(void)loadEnemyAnimations{
    
    [self animationEnemyWalk];
    [self animationEnemyAttack];
    [self animationEnemyStanding];
    [self animationEnemyDamaged];
    [self animationEnemyDie];
}

//Carrega todas as animacoes do inimigo grande
-(void)loadBigEnemyAnimations{
    [self animationBigEnemyWalk];
    [self animationBigEnemyAttack];
    [self animationBigEnemyDamaged];
    [self animationBigEnemyDie];
    [self animationBigEnemyStanding];
}


//Animacoes do inimigo comum
//Cria a animacao de andar
-(void)animationEnemyWalk{
    //Andar
    int numFrameEnemy = 4;
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrameEnemy];
    
    for (int i=1; i<= numFrameEnemy; i++){
        NSString *textureEnemy = [NSString stringWithFormat:@"EnemyAndando/enemyWalking%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [textures addObject:texture];
    }
    _enemyWalkingAnimation = [SKAction animateWithTextures:textures timePerFrame:0.1];
}
//Cria animacao de atacar
-(void)animationEnemyAttack{
    int numFrameEnemy = 5;
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrameEnemy];
    
    for (int i=1; i<= numFrameEnemy; i++){
        
        NSString *textureEnemy = [NSString stringWithFormat:@"EnemyAtacando/enemyAttack%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [textures addObject:texture];
    }
    _enemyAttackingAnimation = [SKAction animateWithTextures:textures timePerFrame:0.1];
}
//Cria animacao de nascer
-(void)animationEnemyBorn{
    int numFrameEnemy = 6;
    float timePerFrame = 0.1;
    
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrameEnemy];
    
    for (int i=1; i<= numFrameEnemy; i++){
        
        NSString *textureEnemy = [NSString stringWithFormat:@"EnemyNascendo/enemyBorning%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [textures addObject:texture];
    }
    _enemyBorningAnimation = [SKAction animateWithTextures:textures timePerFrame:timePerFrame];
    
    //Espera a animacao de nascer acabar
    SKAction *waitAnimation = [SKAction waitForDuration:numFrameEnemy*timePerFrame];
    
    //Muda o estado do inimigo para nao esta nascendo
    SKAction *changeBorningState = [SKAction runBlock:^{
        self.isBorning = NO;
    }];
    
    //Inicia asparticulas do inimigo
    SKAction *initParticles = [SKAction runBlock:^{
        [self initParticles];
    }];
    
    //Sequencia de ações do nascimento;
    SKAction *borningSequence = [SKAction sequence:@[_enemyBorningAnimation, waitAnimation, changeBorningState, initParticles]];
    
    [self runAction:borningSequence];
}

//Cria animacao do inimigo parado
-(void)animationEnemyStanding{
    int numFrameEnemy = 4;
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrameEnemy];
    
    for (int i=1; i<= numFrameEnemy; i++){
        
        NSString *textureEnemy = [NSString stringWithFormat:@"EnemyParado/enemyStanding%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [textures addObject:texture];
    }
    _enemyStandingAnimation = [SKAction animateWithTextures:textures timePerFrame:0.3];
}

//Cria animacao de sofrer dano
-(void)animationEnemyDamaged{
    int numFrameEnemy = 1;
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrameEnemy];
    
    for (int i=1; i<= numFrameEnemy; i++){
        
        NSString *textureEnemy = [NSString stringWithFormat:@"enemy%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [textures addObject:texture];
    }
    _enemyDamegedAnimation = [SKAction animateWithTextures:textures timePerFrame:0.1];
}
//Cria animacao de morte
-(void)animationEnemyDie{
    int numFrameEnemy = 1;
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrameEnemy];
    
    for (int i=1; i<= numFrameEnemy; i++){
        
        NSString *textureEnemy = [NSString stringWithFormat:@"enemy%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [textures addObject:texture];
    }
    _enemyDyingAnimation = [SKAction animateWithTextures:textures timePerFrame:0.1];
}


//Animacoes do inimigo grande
//Cria animacao do inimigo parado
- (void)animationBigEnemyStanding{
    int numFrameEnemy = 4;
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrameEnemy];
    
    for (int i=1; i<= numFrameEnemy; i++){
        
        NSString *textureEnemy = [NSString stringWithFormat:@"bigEnemyParado/bigEnemyStanding%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [textures addObject:texture];
    }
    _bigEnemyStandingAnimation = [SKAction animateWithTextures:textures timePerFrame:0.3];
//    [self runAction:[SKAction repeatActionForever:_bigEnemyStandingAnimation]];
}

//Cria a animacao de andar
-(void)animationBigEnemyWalk{
    //Andar
    int numFrameEnemy = 4;
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrameEnemy];
    
    for (int i=1; i<= numFrameEnemy; i++){
        NSString *textureEnemy = [NSString stringWithFormat:@"BigEnemyAndando/bigEnemyWalking%d",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [textures addObject:texture];
    }
    _bigEnemyWalkingAnimation = [SKAction animateWithTextures:textures timePerFrame:0.2];
}
//Cria animacao de atacar
-(void)animationBigEnemyAttack{
    //Animacao de levantar os bracos
    int numFrameEnemy = 7;
    NSMutableArray *texturesUp = [NSMutableArray arrayWithCapacity:6];
    
    for (int i=1; i<= 3; i++){
        
        NSString *textureEnemy = [NSString stringWithFormat:@"BigEnemyAtacando/bigEnemyAttack%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [texturesUp addObject:texture];
    }
    for (int i=1; i<= 3; i++){
        
        NSString *textureEnemy = [NSString stringWithFormat:@"BigEnemyAtacando/bigEnemyAttack3"];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [texturesUp addObject:texture];
    }
    
    
    _bigEnemyAttackingAnimationUp = [SKAction animateWithTextures:texturesUp timePerFrame:0.15];
    
    
    //Animcao de abaixar os bracos
    NSMutableArray *texturesDown = [NSMutableArray arrayWithCapacity:4];
    
    for (int i=4; i<=numFrameEnemy; i++) {
        NSString *textureEnemy = [NSString stringWithFormat:@"BigEnemyAtacando/bigEnemyAttack%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [texturesDown addObject:texture];
    }
    _bigEnemyAttackingAnimationDown = [SKAction animateWithTextures:texturesDown timePerFrame:0.15];
    
    _waitBigenemyAttackAnimation = [SKAction waitForDuration:0.4];
    
    //Coloca todas as animacoes em uma sequencia
    _bigEnemyAttackingAnimation = [SKAction sequence:@[_bigEnemyAttackingAnimationUp, _bigEnemyAttackingAnimationDown]];
}
//Cria animacao de sofrer dano
-(void)animationBigEnemyDamaged{
    int numFrameEnemy = 1;
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrameEnemy];
    
    for (int i=1; i<= numFrameEnemy; i++){
        
        NSString *textureEnemy = [NSString stringWithFormat:@"enemy%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        
        texture.filteringMode = SKTextureFilteringNearest;
        [textures addObject:texture];
    }
    
    _bigEnemyDamegedAnimation = [SKAction animateWithTextures:textures timePerFrame:0.1];
}
//Cria animacao de morte
-(void)animationBigEnemyDie{
    int numFrameEnemy = 1;
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrameEnemy];
    
    for (int i=1; i<= numFrameEnemy; i++){
        
        NSString *textureEnemy = [NSString stringWithFormat:@"enemy%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [textures addObject:texture];
    }
    _bigEnemyDyingAnimation = [SKAction animateWithTextures:textures timePerFrame:0.1];
}
//Cria animacao de nascer do inimigo grande
-(void)animationBigEnemyBorn{
    int numFrameEnemy = 9;
    float timePerFrame = 0.1;
    
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numFrameEnemy];
    
    for (int i=1; i<= numFrameEnemy; i++){
        
        NSString *textureEnemy = [NSString stringWithFormat:@"bigEnemyNascendo/bigEnemyBorning%i",i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureEnemy];
        texture.filteringMode = SKTextureFilteringNearest;
        [textures addObject:texture];
    }
    _bigEnemyBorningAnimation = [SKAction animateWithTextures:textures timePerFrame:timePerFrame];
    
    //Espera a animacao de nascer acabar
    SKAction *waitAnimation = [SKAction waitForDuration:numFrameEnemy*timePerFrame];
    
    //Muda o estado do inimigo para nao esta nascendo
    SKAction *changeBorningState = [SKAction runBlock:^{
        self.isBorning = NO;
    }];
    
    //Inicia asparticulas do inimigo
//    SKAction *initParticles = [SKAction runBlock:^{
//        [self initParticles];
//    }];
    
    //Sequencia de ações do nascimento;
    SKAction *borningSequence = [SKAction sequence:@[_bigEnemyBorningAnimation, waitAnimation, changeBorningState]];
    
    [self runAction:borningSequence];
}

//Inicia as particulas do inimigo comum
-(void)initParticles{
    self.comumParticles = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"normalEnemyParticle" ofType:@"sks"]];
    
    self.comumParticles.position = CGPointMake(0,(self.size.height/5));
    self.comumParticles.targetNode = self.parent;
    
    [self addChild:self.comumParticles];
}

//Cria paticulas de dano
-(void)initDamageParticlesInPositon:(CGPoint)position{
    self.damageParticles = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"enemyDamageParticle" ofType:@"sks"]];
    
    //Aparece na direcao do ataque
    self.damageParticles.position = position;
    
    //Faz as particulas aparecerem uma vez
    [self.parent addChild:self.damageParticles];
    [self.damageParticles resetSimulation];
}


//Atualizar a posiçao do VisualBoundingBox
- (void) updateVisualBoundingBoxCenter{
    
    CGPoint center = CGPointMake(self.position.x, self.position.y + self.size.height/2);
    self.visualBoundingBox.position = center;
    
}
@end
