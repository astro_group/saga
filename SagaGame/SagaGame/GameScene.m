//
//  GameScene.m
//  SagaGame
//
//  Created by Naiara Moura on 07/04/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import "GameScene.h"
#import "Text.h"

#define DECREESE_TIME_BETWEEN_ENEMY_SPAWN 0.25
#define SCORE 50

@implementation GameScene
{
    //Layers
    SKNode *_principalLayer;
    SKNode *_secondLayer;
    SKNode *_thirdLayer;
    SKNode *_rocksLayer;
    SKNode *_shakeLayer;
    
    //Sprites Background
    SKSpriteNode *_background;
    SKSpriteNode *_floor;
    SKSpriteNode *_rocks;
    SKSpriteNode *_clouds;
    
    //Variaveis auxiliares de tempo
    NSTimeInterval _lastTimeUpdated;
    NSTimeInterval _dt;
    
    //Variávei auxiliares de borda
    CGPoint _topRight;
    CGPoint _bottonLeft;
    CGFloat _floorHeight;
    CGFloat _rigthBorder;

    //Personagens
    SGPlayer *_player;
    CGPoint _playerVelocity;
    CGPoint _atualPlayerVelocityInverseX;
    CGPoint _atualPlayerVelocityX;
    
    //Swipe
    UISwipeGestureRecognizer *_recornizer;

    //Variaveis de Fisica
    CGPoint _gravity;

    //Tap
    UITapGestureRecognizer *_tap;
    SKSpriteNode *visualBox;//Caixa de colisao do ataque
    SKSpriteNode *visualEnemyAttackBox; //Caixa de colisao do ataque do inimigo
    NSMutableArray *attackBoxsArray; //Array de caixas para serem apagadas
    
    //Actions de spawn do inimigo
    CGFloat _timeBetweenEnemySpawn;
    SKAction *spawnEnemy;
    SKAction *waitToSpawnAgain;
    SKAction *sequenceSpawnEnemy;
    SKAction *repeatSpawnEnemy;
    SKAction *minTimeWaitToSpawnAgain;
    SKAction *changeTimeBetweenEnemySpawn;
    
    //Actions de spawn do inimigo Grande
    CGFloat _timeBetweenBigEnemySpawn;
    SKAction *spawnBigEnemy;
    SKAction *changeTimeBetweenBigEnemySpawn;
    SKAction *waitToSpawnBigEnemyAgain;
    SKAction *sequenceSpawnBigEnemy;
    
    //Ation da maquina de estados do inimigo
    float distanceBetweenEnemyPlayer;//Distancia entre o jogador e o player
    SKAction *moveToPlayerPosition;
    
    //Morte do inimigo comum
    int scorePointsFromDeath;
    
    //labls de pontuacao, life e combo
    SKLabelNode *scoreNumLabel;
    SKLabelNode *lifeNumLabel;
    SKLabelNode *comboNumLabel;
    
    //Variaveis de pontuação
    int _score;
    
    //Scala das imagens.
    CGFloat scale;
    
    //Player Animations
    SKAction *_playerWalkingAnimation;
    SKAction *_playerAttackingAnimation;
    SKAction *_playerDamagedAnimation;
    
    SKSpriteNode *_leftBound;
    SKSpriteNode *_rigthBound;
    
    //Particulas
    SKEmitterNode *_windParticles;
}

-(id) initWithSize:(CGSize)size{
    CGPoint anchorCenter = CGPointMake(0.5, 0.5);//Centro da imagem pra ancora.
    
    _score =0;
    
    if (self = [super initWithSize:size]) {
        self.backgroundColor = [SKColor purpleColor];
        
        //Inicializando musica do jogo
        NSURL *urlMusic = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/GameMusic.wav", [[NSBundle mainBundle] resourcePath]]];
        NSError *error;
        
        self.gameMusic = [[AVAudioPlayer alloc] initWithContentsOfURL:urlMusic error:&error];
        self.gameMusic.numberOfLoops = -1;
        
        if (!self.gameMusic)
            NSLog(@"%@",[error localizedDescription]);
        else
            [self.gameMusic play];
        
        //Inicializando layers
        _shakeLayer = [SKNode node];
        [self addChild:_shakeLayer];
        
        //Inicializando terceira layer
        _thirdLayer = [SKNode node];
        [_shakeLayer addChild:_thirdLayer];
        
        //Inicializando segunda layer
        _secondLayer = [SKNode node];
        [_shakeLayer addChild:_secondLayer];
        
        //Inicializando layer principal
        _principalLayer = [SKNode node];
        [_shakeLayer addChild:_principalLayer];
        
        _rocksLayer = [SKNode node];
        [_shakeLayer addChild:_rocksLayer];
        
        //Colocando os backgrounds.
        CGRect imageSize = CGRectMake(0.0, 0.0, 2668.0, 750.0);
        
        //Colocando o background na terceira layer.
        _background = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"background1"] size:imageSize.size];
        _background.anchorPoint = CGPointZero;
        _background.position = CGPointMake(-200, 0);
        _background.scale = self.size.height/_background.size.height;
        [_thirdLayer addChild:_background];
        
        //Colocando o Nuvens menores atrás na segunda layer.
        _background = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"background6"] size:imageSize.size];
        _background.anchorPoint = CGPointZero;
        _background.position = CGPointMake(0.0, 100);
        _background.scale = self.size.height/_background.size.height/2;
        [_thirdLayer addChild:_background];
        
        //Colocando o background na segunda layer.
        _background = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"background2"] size:imageSize.size];
        _background.anchorPoint = CGPointZero;
        _background.position = CGPointZero;
        _background.scale = self.size.height/_background.size.height;
        [_thirdLayer addChild:_background];
        
        //Colocando o Nuvens maiores à frente na segunda layer.
        _background = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"background6"] size:imageSize.size];
        _background.anchorPoint = CGPointZero;
        _background.position = CGPointZero;
        _background.scale = self.size.height/_background.size.height;
        [_thirdLayer addChild:_background];
        
        //Colocando o background na layer principal.
        _background = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"background3"] size:imageSize.size];
        _background.anchorPoint = CGPointZero;
        _background.position = CGPointZero;
        _background.scale = self.size.height/_background.size.height;
        [_secondLayer addChild:_background];
        
        //Scala das imagens.
        scale = self.size.height/_background.size.height;
        
        //Colocando o chao.
        _floor = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"background4"] size:CGSizeMake(2668.0, 250)];
        _floor.texture.filteringMode = SKTextureFilteringNearest;
        _floor.scale = scale;
        _floor.anchorPoint = anchorCenter;
        _floor.position = CGPointMake(self.size.width/2, _floor.size.height/2);
        [_principalLayer addChild:_floor];
        
        //Colocar rochas depois de tudo
        
        //Colocando o personagem.
        CGRect rectOfPlayer = CGRectMake(0, 0, 236.0, 224.0);
        _player = [SGPlayer spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"PlayerAndando/walk1"] size:rectOfPlayer.size];
        _player.direction = RIGHT;
        _player.attackingHand = RIGHT;
        _player.scale = scale/1.7;
        _player.anchorPoint = CGPointMake(0.5, 0);
        _player.position = CGPointMake((self.size.width - _player.size.width/2)/2, _floorHeight +1);
        _player.velocityXDefault = 200;
        _player.velocityXReduced = _player.velocityXDefault/4;
        _player.velocity = CGPointMake(_player.velocityXDefault, 0);
        _player.hp = 5;
        _player.isInCombo = NO;
        _player.impulse = CGPointMake(0, 1000);
        //Definir Caixa de colisao do player
        CGSize BBSize = CGSizeMake(_player.size.width*0.5, _player.size.height*0.75);
        _player.visualBoundingBox = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:BBSize];
        _player.visualBoundingBox.alpha = 0.0;
        [_player updateVisualBoundingBoxCenter];
        [_principalLayer addChild:_player.visualBoundingBox];
        
        //Carregar Sons do player
        _player.attackSound = [SKAction playSoundFileNamed:@"Attack_scream.wav" waitForCompletion:NO ];
//        _player.dieSound = [SKAction playSoundFileNamed:@"Morreu.wav" waitForCompletion:NO];
//        _player.walkSound = [SKAction playSoundFileNamed:@"playerWalk.wav" waitForCompletion:NO];
//        _player.sufferSound = [SKAction playSoundFileNamed:@"Sofreu.wav" waitForCompletion:NO];
        _player.hitEnemySound = [SKAction playSoundFileNamed:@"Attack_hit.wav" waitForCompletion:NO];
//        _player.jumpSound = [SKAction playSoundFileNamed:@"playerJump.wav" waitForCompletion:NO];
        
        //Adicionar player à layer principal
        [_principalLayer addChild:_player];
        
        //Definindo bordas
        _topRight = CGPointMake(_background.size.width, _background.size.height);
        _bottonLeft = _background.position;
        
        //Definindo Variaveis de fisica
        _gravity = CGPointMake(0, -45);
        
        //Colocando Pedras
        _rocks = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"background5"] size:CGSizeMake(self.size.width*3, 200.0)];
        _rocks.texture.filteringMode = SKTextureFilteringNearest;
        _rocks.anchorPoint = CGPointZero;
        _rocks.position = CGPointZero;
        [_rocksLayer addChild:_rocks];
        
        
        
        //Actions de spawn enemy
        _timeBetweenEnemySpawn = 6.0; //Tempo entre nascimento de inimigos inicial
        spawnEnemy = [SKAction runBlock:^{
            [self spawnEnemy];
            [self spawnEnemy];
            [self spawnEnemy];
        }];
        minTimeWaitToSpawnAgain = [SKAction waitForDuration:2.0];
        waitToSpawnAgain = [SKAction waitForDuration:_timeBetweenEnemySpawn];
        changeTimeBetweenEnemySpawn = [SKAction runBlock:^{
            _timeBetweenEnemySpawn *= DECREESE_TIME_BETWEEN_ENEMY_SPAWN;
        }];
        sequenceSpawnEnemy = [SKAction sequence:@[minTimeWaitToSpawnAgain, spawnEnemy, waitToSpawnAgain, changeTimeBetweenEnemySpawn]];
        
        [self runAction:[SKAction repeatActionForever:sequenceSpawnEnemy]];
        
        //Actions de spawn do inimigo grande
        _timeBetweenBigEnemySpawn = 10.0;
        spawnBigEnemy = [SKAction runBlock:^{
            [self spawnBigEnemy];
        }];
        waitToSpawnBigEnemyAgain = [SKAction waitForDuration:_timeBetweenBigEnemySpawn];
        changeTimeBetweenBigEnemySpawn = [SKAction runBlock:^{
            _timeBetweenBigEnemySpawn *= DECREESE_TIME_BETWEEN_ENEMY_SPAWN;
        }];
        sequenceSpawnBigEnemy = [SKAction sequence:@[waitToSpawnBigEnemyAgain, spawnBigEnemy, changeTimeBetweenBigEnemySpawn, minTimeWaitToSpawnAgain]];
        
        [self runAction:[SKAction repeatActionForever:sequenceSpawnBigEnemy]];
        
        //Variaveis do sistema de combos
        _player.lastTimeUpdateCombo = 0;
        _player.timeSinceLastAttack = 0;
        _player.comboNumber = 1;
        
        //adicicionando as labels de score e life-----------------------------------------
            //pontuação
        SKLabelNode *scoreLabel = [Text labelNodeWithFontNamed:@"PressStart2P"];
        scoreLabel.position = CGPointMake(self.position.x + 60, self.size.height - 25);
        scoreLabel.fontSize = 15.0;
        scoreLabel.text = @"SCORE:";
        scoreLabel.name = @"scoreLabel";
        [self addChild:scoreLabel];
        
        scoreNumLabel = [Text labelNodeWithFontNamed:@"PressStart2P"];
        scoreNumLabel.position = CGPointMake(self.position.x + 120, self.size.height - 25);
        scoreNumLabel.fontSize = 15.0;
        scoreNumLabel.text = @"0";
        scoreNumLabel.name = @"scoreNumLabel";
        [scoreNumLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
        [self addChild:scoreNumLabel];
        
        //Combo
//        SKLabelNode *comboLabel = [SKLabelNode labelNodeWithFontNamed:@"Thirteen Pixel Fonts"];
//        comboLabel.position = CGPointMake(self.size.width -150, self.position.y + 15);
//        comboLabel.fontSize = 20.0;
//        comboLabel.text = @"Combo:";
//        comboLabel.name = @"comboLife";
        
//        [self addChild:comboLabel];
        
        comboNumLabel = [Text labelNodeWithFontNamed:@"Fipps"];
         comboNumLabel.position = CGPointMake(50.0, self.size.height - 120);
         comboNumLabel.fontSize = 40.0;
         comboNumLabel.text = [[NSString alloc] initWithFormat:@"%d",_player.comboNumber];
         comboNumLabel.name = @"comboLife";
        comboNumLabel.fontColor = [SKColor redColor];
        [comboNumLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
        [self addChild:comboNumLabel];
        //-------------------------------------------------------------------------------
        
        //Morte do inimigo comum
        scorePointsFromDeath = SCORE;
        
        //Desenha a quantidade inicial de life
        [self DrawPlayerLifeOnScreen];
        
        //Player Animation
        [self loadPlayerAnimations];
        [_player startWalkingAnimation];
        
        //Definido a altura do chao
        _floorHeight = self.size.height/7;
        
        //Definir limites da tela
        _rigthBorder = (CGFloat) -(_background.size.width -self.size.width);
        //O outro limite é igual a 0.0
        
        //Particulas da tela
        [self createWindParticles];
    }
    
    return self;
}



//Cria os swipes e tap
-(void)didMoveToView:(SKView *)view{
    
    _recornizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    _recornizer.direction = UISwipeGestureRecognizerDirectionUp;
    [[self view] addGestureRecognizer:_recornizer];
    
    _recornizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    _recornizer.direction = UISwipeGestureRecognizerDirectionRight;
    [[self view] addGestureRecognizer:_recornizer];
    
    _recornizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    _recornizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [[self view] addGestureRecognizer:_recornizer];
    
    _tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:_tap];
}

//Main loop do jogo
-(void) update:(NSTimeInterval)currentTime{
   
    //Calculo do tempo
    if(_lastTimeUpdated){
        _dt = currentTime - _lastTimeUpdated;
        
        if((double) _dt > 1){
            _dt = 0.04;
        }
    }
    else{
        _dt = 0;
    }
    _lastTimeUpdated = currentTime;
    
    //Velocidade das layers
    CGPoint layerVelocity = CGPointMake(-_player.velocity.x, 0.0);
    CGPoint secondLayerVelocity = CGPointMake(-_player.velocity.x/2, 0.0);
    CGPoint thirdLayerVelocity = CGPointMake(-_player.velocity.x/3, 0.0);

    [self moveSprite:_player withVelocity:_player.velocity];
    
    if(_player.position.x > self.size.width/2 && _player.position.x < _background.size.width - self.size.width/2){
        
        //Move as layers
        [self moveSprite:_principalLayer withVelocity: layerVelocity];
        [self moveSprite:_rocksLayer withVelocity:layerVelocity];
        [self moveSprite:_secondLayer withVelocity: secondLayerVelocity];
        [self moveSprite:_thirdLayer withVelocity: thirdLayerVelocity];

        
        //Atualiza posiçao da tela caso passe dos limites
        if(_principalLayer.position.x > 0.0){
            _principalLayer.position = CGPointMake(0.0, _principalLayer.position.y);
        }
        
        
        
        if(_principalLayer.position.x< _rigthBorder){
            _principalLayer.position = CGPointMake(_rigthBorder, _principalLayer.position.y);
        }
    }
    
    
    [self checkPlayerStatus];
    
    [self applyGravity];
    [self boundsCheckPlayer];
    
    //Atualiza estado dos inimigos
    [self updateEnemyIAEstate];
    
    //Comeca o timer do combo
    if (_player.isInCombo) {
        if (_player.lastTimeUpdateCombo) {
            _player.timeSinceLastAttack += currentTime - _player.lastTimeUpdateCombo;
        } else {
            _player.timeSinceLastAttack = 0;
        }
        
        _player.lastTimeUpdateCombo = currentTime;
        
        //Tira o player do combo se passar 5s sem atacar
        if (_player.timeSinceLastAttack > 5) {
            _player.lastTimeUpdateCombo = 0;
            _player.isInCombo = NO;
            _player.comboNumber = 0;
            //Atualiza o numero na tela
            comboNumLabel.text = [[NSString alloc] initWithFormat:@"%d", _player.comboNumber];
        }
    } else {
        _player.comboNumber = 0;
        //Atualiza o numero na tela
        comboNumLabel.text = [[NSString alloc] initWithFormat:@"%d", _player.comboNumber];
    }

//    //Desenhar Life do player na tela
//    [self DrawPlayerLifeOnScreen];
    
    //Se life acabar chama gameOver
    if (_player.hp <= 0) {
         [self GameOver];
    }

    //Move o attack box do inimigo grande
    [self moveBigEnemyAttackBox];
    
    //Checar caso o player esteja em Queda livre e mudar animaçao
    [self checkIfPlayerIsOnFreeFall];
    
    //Checar se o player esta invalido e no chao
    if (_player.isInvalid == YES) {
        [_player recoverFromInvalidationInFloorHight:(_floorHeight+1)];
    };
    
    //Checar status do combo
    [self checkStatusComboLabel];
    
    //NSLog(@"tempo entre inimigos: %f", _timeBetweenEnemySpawn);
    
//    NSLog(@" posicao background x:%f y:%f \rTopRight x:%f y:%f \rScene TopRight x:%f y:%f \rPrincipal TopRight x:%f y:%f",_background.position.x,_background.position.y, _topRight.x, _topRight.y, self.size.width, self.size.height, _principalLayerTopRight.x, _principalLayerTopRight.y);
    
//    NSLog(@"Player x:%f y:%f", _player.position.x, _player.position.y);
//    NSLog(@"Is in combo: %d", _player.isInCombo);
    //NSLog(@"Life: %d",_player.hp);
//    NSLog(@"Player direction: %i",_player.direction);

//    NSLog(@"Life: %d",_player.hp);
//    NSLog(@"Floor Height: %f",_floorHeight+1);
    
    
}


//Gestos de swipe
- (void)handleSwipe: (UISwipeGestureRecognizer *)sender{
    
    //Swipe para cima
    if (sender.direction == UISwipeGestureRecognizerDirectionUp){
        
        //Checar se o Player esta no solo
        if([self checkSpriteinGround:_player]){
            
            //Mandar player pular
            [_player jump];
            
//            self.view.paused = !self.view.paused;
        }
    }
    //Swipe para esquerda
    if( sender.direction == UISwipeGestureRecognizerDirectionLeft){
        if(_player.direction != LEFT){
            _player.direction = LEFT;
            _player.velocity = [SGVector invertXFromVector:_player.velocity];
            _player.xScale *= -1;
        
        //Faz o player dar dash
        } else if (_player.direction == LEFT && [self checkSpriteinGround:_player]) {
            [_player dash];
        }
    }
    //Swipe para direita
    if (sender.direction ==UISwipeGestureRecognizerDirectionRight) {
        if(_player.direction != RIGHT){
            _player.direction = RIGHT;
            _player.velocity = [SGVector invertXFromVector:_player.velocity];
            _player.xScale *= -1;
            
        //Faz o player dar dash
        } else if (_player.direction == RIGHT && [self checkSpriteinGround:_player]) {
            [_player dash];
        }
    }
}

//Gesto de Tap
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    
    [_player attack];
    [self checkCollisionPlayerAttackBoxWithEnemies];
}

//Mover sprite com velocidade
- (void) moveSprite:(SKNode *) sprite
       withVelocity:(CGPoint) velocity
{
    CGPoint amountToMove = [SGVector multiplyVector:velocity withScalar:_dt];
    sprite.position = [SGVector sumVector:sprite.position and:amountToMove];
}

//Mover point com velocidade
- (void) movePoint:(CGPoint*)point
      withVelocity:(CGPoint)velocity{
    CGPoint amountToMove = [SGVector multiplyVector:velocity withScalar:_dt];
    *point = [SGVector sumVector:*point and:amountToMove];
}

//Mover layer com velocidade velocity
- (void) moveLayer: (SKNode *)layer withVelocity: (CGPoint)velocity{
    
    CGPoint amountToMove = [SGVector multiplyVector:velocity withScalar:_dt];
    layer.position = [SGVector sumVector:layer.position and:amountToMove];
    
}

//Mover point com velocidade angular
- (CGPoint) rotatePoint:(CGPoint)point aroundCenter:(CGPoint)center withAngularVelocity:(CGFloat)velocity{
    CGFloat amountToMove = velocity * _dt;
    CGPoint result = [SGVector rotateVector:point WithCenter:center FromAngle:amountToMove];
    
    return result;
}

//Checar sprite no chao
- (BOOL)checkSpriteinGround:(SKSpriteNode *) sprite{
    
    if(sprite.position.y < self.size.height/5 + 1){
        return YES;
    }
    return NO;
}

//Funcoes de fisica
- (void) applyGravity{
    
    _player.velocity = CGPointMake(_player.velocity.x, _player.velocity.y + _gravity.y);

}

//Controle de borda
- (void)boundsCheckPlayer {
    
    //Checar bordas Laterais
    //Lateral direita
    if ( _player.position.x  >= _topRight.x) {
        _player.direction = LEFT;
        _player.velocity = [SGVector invertXFromVector:_player.velocity];
        _player.xScale *= -1;
      //Lateral esquerda
    } else if (_player.position.x <= _bottonLeft.x){
        _player.direction = RIGHT;
        _player.velocity = [SGVector invertXFromVector:_player.velocity];
        _player.xScale *= -1;
    }
    
    //Caso ultrapasse a borda esquerda
    if(_player.position.x < _bottonLeft.x){
        _player.position = CGPointMake(_bottonLeft.x +1, _player.position.y);
    }
    
    if(_player.position.x> _topRight.x){
        _player.position = CGPointMake(_topRight.x +1, _player.position.y);
    }
    
//    NSLog(@"%f %f", _player.position.x, _topRight.x);
    
    //Checar Chao
    if(_player.position.y < _floorHeight){
        
        _player.velocity = CGPointMake(_player.velocity.x, 0);
        
        //Caso o Player esteja no chao e nao esteja andando e nem atacando
        if( _player.animationStatus != WALKING)
        {
            //Iniciar animaçao de andar
            [_player startWalkingAnimation ];
        }
        
        //Caso o player ultrapasse o chao
        if(_player.position.y < _floorHeight -1){
            _player.position = CGPointMake(_player.position.x, _floorHeight);
        }
    }
}

//Cria inimigo normal em um x aleatorio do cenario
- (void)spawnEnemy{
    CGRect rect = CGRectMake(0, 0, 47.0, 47.0);
    SGEnemy *enemy = [SGEnemy spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"Enemy/enemyStanding1"] size:rect.size]; //Cria inimigo
    
    //Gera um x dentro das bordas e define a posicao do inimigo e escala
    enemy.scale = _player.size.height/enemy.size.height;
    enemy.anchorPoint = CGPointMake(0.5, 0);
    CGFloat randomPositionX = arc4random() % (int)(_topRight.x - enemy.size.width);
    enemy.position = CGPointMake( randomPositionX, _floorHeight);
    
    //Define a velocidade, o hp e a direcao do inimigo
    enemy.name = @"enemy";
    enemy.hp = 2;
    enemy.direction = RIGHT;
    enemy.velocity = CGPointZero;
    enemy.isAlive = YES;
    enemy.isAttacking = NO;
    enemy.isBorning = YES;
    enemy.isWalking = NO;
    enemy.isStandng = NO;
    enemy.attackBoxWidth = 45;
    enemy.attackBoxHeigth = 15;
    enemy.velocityXDefault = 100;
    enemy.damage = 1;
    
    enemy.damageImpulse = CGPointMake(40, _player.velocity.y);
    enemy.delayResponseIA = 1;
    
    enemy.distanceBetweenEnemyPlayer = 100;
    
    moveToPlayerPosition = [SKAction moveTo:CGPointMake(_player.position.x, self.size.height/5) duration:4];
    enemy.enemyVisionArea = self.size.width/3.2;
    enemy.enemyAttackArea = self.size.width/10;
    
    
    //Inicializar Caixa de colisao do inimigo
    enemy.visualBoundingBox = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(enemy.size.width/2, enemy.size.height)];
    enemy.visualBoundingBox.anchorPoint = CGPointMake(0.5, 0.5);
    [enemy updateVisualBoundingBoxCenter];
    enemy.visualBoundingBox.alpha = 0;
    [_principalLayer addChild:enemy.visualBoundingBox];
    //Carregar Sons
//    enemy.attackSound = [SKAction playSoundFileNamed:@"enemeAttack.wav" waitForCompletion:NO ];
//    enemy.dieSound = [SKAction playSoundFileNamed:@"enemyDie.wav" waitForCompletion:NO];
//    enemy.walkSound = [SKAction playSoundFileNamed:@"enemyWalk.wav" waitForCompletion:YES];
//    enemy.sufferSound = [SKAction playSoundFileNamed:@"Sofreu.wav" waitForCompletion:NO];
//    enemy.bornSound = [SKAction playSoundFileNamed:@"Nasceu.wav" waitForCompletion:NO];
//    enemy.standingSound = [SKAction playSoundFileNamed:@"enemyStanding.wav" waitForCompletion:NO];
    
    //Carrega as animacoes
    [enemy loadEnemyAnimations];
    
    //Adicionar inimigo na layer principal do jogo
    [_principalLayer addChild:enemy];
    
    //Faz a animacao do inimigo nascer
    [enemy animationEnemyBorn];
    
    //Rodar som de nascimento
    [self runAction:enemy.bornSound];
}

//Cria inimigo grande em um x aleatorio do cenario
- (void)spawnBigEnemy{
    CGRect rect = CGRectMake(0, 0, 100.0, 75.0);
    SGEnemy *bigEnemy = [SGEnemy spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"bigEnemyStanding1"] size:rect.size]; //Cria inimigo
    
    //Gera um x dentro das bordas e define a posicao do inimigo e escala
    bigEnemy.scale = _player.size.height/bigEnemy.size.height;
    bigEnemy.anchorPoint = CGPointMake(0.5, 0);
    CGFloat randomPositionX = arc4random() % (int)(_topRight.x - bigEnemy.size.width);
    bigEnemy.position = CGPointMake( randomPositionX, _floorHeight);
    
    //animação do inimigo
    [bigEnemy animationEnemyWalk];
    
    //Define a velocidade, o hp e a direcao do inimigo
    bigEnemy.name = @"bigEnemy";
    bigEnemy.hp = 5;
    bigEnemy.direction = arc4random() % 2;
    bigEnemy.velocity = CGPointZero;
    bigEnemy.isAlive = YES;
    bigEnemy.isAttacking = NO;
    bigEnemy.isBorning = YES;
    bigEnemy.scale = (self.size.height/bigEnemy.size.height)*1.5;//(_player.size.height/bigEnemy.size.height);
    bigEnemy.attackBoxWidth = 150;
    bigEnemy.attackBoxHeigth = 50;
    
    bigEnemy.damageImpulse = CGPointMake(1000, 500);
    bigEnemy.delayResponseIA = 1;
    
    bigEnemy.angularVelocityBigEnemyAttack = M_PI*2;
    
    bigEnemy.damage = 2;
    bigEnemy.velocityXDefault = 70;
    
    bigEnemy.distanceBetweenEnemyPlayer = 300;
    
    moveToPlayerPosition = [SKAction moveTo:CGPointMake(_player.position.x, self.size.height/5) duration:4];
    bigEnemy.enemyVisionArea = self.size.width/1.5;
    bigEnemy.enemyAttackArea = self.size.width/2;
    
    
    //Inicializar Caixa de colisao do inimigo
    bigEnemy.visualBoundingBox = [SKSpriteNode spriteNodeWithColor:[UIColor purpleColor] size:CGSizeMake(bigEnemy.size.width*0.4, bigEnemy.size.height*0.7)];
    bigEnemy.visualBoundingBox.anchorPoint = CGPointMake(0.5, 0.5);
    [bigEnemy updateVisualBoundingBoxCenter];
    bigEnemy.visualBoundingBox.alpha = 0;

    [_principalLayer addChild:bigEnemy.visualBoundingBox];
    
    //Carregar Sons
//    bigEnemy.attackSound = [SKAction playSoundFileNamed:@"bigEnemyAttack.wav" waitForCompletion:NO ];
//    bigEnemy.dieSound = [SKAction playSoundFileNamed:@"bigEnemyDie.wav" waitForCompletion:NO];
//    bigEnemy.walkSound = [SKAction playSoundFileNamed:@"bigEnemyWalk.wav" waitForCompletion:NO];
//    bigEnemy.sufferSound = [SKAction playSoundFileNamed:@"bigEnemySuffer.wav" waitForCompletion:NO];
//    bigEnemy.bornSound = [SKAction playSoundFileNamed:@"bigEnemyBorn.wav" waitForCompletion:NO];
    
    //Carrega as animacoes
    [bigEnemy loadBigEnemyAnimations]; 
    
    //Adicionar Inimigo grande à layer principal
    [_principalLayer addChild:bigEnemy];
    
    //Faz a animação do inimigo grande nascer
    [bigEnemy animationBigEnemyBorn];
    
    //Rodar som de nascimento
    [self runAction:bigEnemy.bornSound];
}


//Ativa a maquina de estados dos Inimigos
- (void)updateEnemyIAEstate{
    
    //Maquina de estados para inimigo Normal
    [_principalLayer enumerateChildNodesWithName:@"enemy" usingBlock:^(SKNode *node, BOOL *stop) {
        SGEnemy *enemy = (SGEnemy *)node;
        
        [enemy updateVisualBoundingBoxCenter];
  
        //Atualizar o estado do inimigo
        BOOL enemyAttack = [enemy stateOfEnemyRelativeToPlayer:_player.position];
        
        //Caso o inimigo ataque
        if(enemyAttack)
        {
            //inimigo tenta atacar
            [enemy attack];
        }
        
        //Checar a colisao com o ataque enquanto a caixa estiver criada
        if(enemy.visualAttackBox && !_player.isInvunerable){
            //Checar colisao do ataque
            [self checkCollisionEnemyAttackBox:enemy withPlayer:_player];
        }
        
        //Move inimigo
        [self moveSprite:enemy withVelocity:enemy.velocity];
    }];
    
    //Maquina de estados para inimigo Maior
    [_principalLayer enumerateChildNodesWithName:@"bigEnemy" usingBlock:^(SKNode *node, BOOL *stop) {
        SGEnemy *bigEnemy = (SGEnemy *)node;
        [bigEnemy stateOfEnemyRelativeToPlayer:_player.position];
        
        [bigEnemy updateVisualBoundingBoxCenter];
        
        //Atualizar o estado do inimigo
        BOOL enemyAttack = [bigEnemy stateOfEnemyRelativeToPlayer:_player.position];
        
        //Caso o inimigo ataque
        if(enemyAttack)
        {
                //inimigo inicia o ataque
                [bigEnemy attack];
        }
        
        if (bigEnemy.isAttacking) {
            if (bigEnemy.visualAttackBox.position.y > _floorHeight + bigEnemy.visualAttackBox.size.height) {
                //Checar colisao do ataque
                if(bigEnemy.visualAttackBox && !_player.isInvunerable){
                    [self checkCollisionEnemyAttackBox:bigEnemy withPlayer:_player];
                }
            }
        }
        
        //Move inimigo
        [self moveSprite:bigEnemy withVelocity:bigEnemy.velocity];
    }];
}

//Mata o inimigo e incrementa a pontuação
-(void)killEnemy:(SGEnemy *)enemy {


    enemy.isAlive = NO;
    
    //Metodo que faz aparecer a pontuação acima do inimigo
    [self labelEnemykill:scorePointsFromDeath withEnemy:enemy];
    
    //Calcula a pontuacao q sera incrementada
    if (_player.comboNumber == 0) {
        scorePointsFromDeath = SCORE;
        
    } else {
        NSLog(@"%d", _player.comboNumber);
        scorePointsFromDeath = SCORE * _player.comboNumber;
    }
    
    //Animaçao de morte
    [enemy enemyDeath];
    
    //Aumenta os pontos e atualiza aos pontos no label
    _score += scorePointsFromDeath;
    NSString *aux = [NSString stringWithFormat:@"%i",_score];
    scoreNumLabel.text = aux;
    
    //Desalocar memória do inimigo
    enemy = nil;
}

//Desenha o life na tela
-(void)DrawPlayerLifeOnScreen{
    //define o tamanho do rect da imagem
    CGRect rect = CGRectMake(0, 0, 36.0, 48.0);
    
    for (int i = 1; i <= _player.hp; i++) {
        Life *hp = [Life spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"Flame/flame1"] size:rect.size];
        
        hp.name = @"flame";
        hp.hpNumber = i;
        hp.scale = (self.size.height/6)/hp.size.height;
        hp.anchorPoint = CGPointZero;
        hp.position = CGPointMake(self.size.width - (hp.size.width*i), self.size.height - hp.size.height);
        
        [hp loadConstantAnimation];
        [hp runAction:[SKAction repeatActionForever:hp.constantAnimation]];
        
        [self addChild:hp];
    }
}

//Destroi a chama de acordo com o hp do player
-(void)destroyLife{
        [self enumerateChildNodesWithName:@"flame" usingBlock:^(SKNode *node, BOOL *stop) {
            Life *hp = (Life*) node;
            
            if (hp.hpNumber >= _player.hp+1) {
                [hp removeFromParent];
            }
            
        }];
}

//Carrega todas as animaçoes do player
-(void)loadPlayerAnimations{
    
    //Carregar animaçao de andar
    [_player loadPlayerWalkingAnimations];
    
    //Carregar animaçao de Attack
    [_player loadPlayerAttackAnimation];
    
    //Carregar animaçao hitted
    [_player loadPlayerHittedAnimation];
    
    //Carregar animaçao de Pulo
    [_player loadPlayerJumpingAnimation];
    
    //Carregar animaçao de queda
    [_player loadPlayerFallingAnimation];
}

//Ir tela de game Over
-(void)GameOver{
    
        SKAction * wait = [SKAction waitForDuration:0.0]; SKAction * block =
        [SKAction runBlock:^{ GameOverScene * gameOver =
            [[GameOverScene alloc] initWithSize:self.size]; SKTransition *reveal =
            [SKTransition fadeWithColor:[UIColor colorWithRed:140.0/255.0
                                                        green:171.1/255.0
                                                         blue:186.0/255.0
                                                        alpha:2]
                               duration:1.0];
            [self.view presentScene:gameOver transition: reveal]; [self writePlistScore:_score];gameOver.valorRecebidoScore = _score;}];
        [self runAction:[SKAction sequence:@[wait, block]]];
    
    [self.gameMusic stop];
    
}


//Gravação da pontuação em uma plist
-(void)writePlistScore:(int)score{
    
   //Caminha para onde vai o arquivo

    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/dadosJogo.plist"];
   NSMutableArray *gravar= [[NSMutableArray alloc]init];
    
    //transformando score em um NSNumber
    NSNumber *scoreAtual = [NSNumber numberWithInt:score];
    
   //Acrescenta score ao score do arquivo atual
    gravar = [[NSMutableArray alloc] initWithContentsOfFile:path];
    [gravar  addObject:scoreAtual];
    
    //Caso não exista a plist
    if (gravar == nil) {
        gravar = [[NSMutableArray alloc] init];
        [gravar addObject:scoreAtual];
    }
    
    [gravar writeToFile:path atomically:YES];
}


//Checa se a caixa de colisao do inimigo bateu no player
- (void) checkCollisionEnemyAttackBox:(SGEnemy *) enemy withPlayer:(SGPlayer *) player{
    
    //Checar se acertou o box do player
    if(CGRectIntersectsRect(_player.visualBoundingBox.frame, enemy.visualAttackBox.frame) && (!_player.isInvalid || !_player.isInvunerable)){
        
        //Invalidar Player
        [_player invalidatePlayerRelativeToEnemy:enemy.direction withImpulse:enemy.damageImpulse];
        
//        NSLog(@"Player acertado");
        _player.hp -= enemy.damage;
        _player.isInCombo = NO;
        
        //Fechar caixa de colisao caso encontrar player
        [enemy.visualAttackBox removeFromParent];
        enemy.visualAttackBox = nil;
        
        [self enumerateChildNodesWithName:@"heart" usingBlock:^(SKNode *node, BOOL *stop) {
            SKSpriteNode *heart = (SKSpriteNode *) node;
            
            [heart removeFromParent];
        }];
        
        //DAR UM SHAKE
        if([enemy.name isEqualToString:@"bigEnemy"])
            [self shakeScreen:100];
        
        //Destroi a chama de acordo com o hp do player
        [self destroyLife];
        
        //Sobe um pouco a posicao do player
        _player.position = CGPointMake(_player.position.x, _player.position.y +2);
    }
}

//Move o attack box do inimigo grande
- (void) moveBigEnemyAttackBox{
    [_principalLayer enumerateChildNodesWithName:@"bigEnemy" usingBlock:^(SKNode *node, BOOL *stop) {
        SGEnemy *bigEnemy = (SGEnemy *)node;
        
        CGPoint newOrigin;
        //Move enquanto a caixa estiver acima do chão.
        if (bigEnemy.visualAttackBox.position.y >= _floorHeight + bigEnemy.visualAttackBox.size.height) {
            if (bigEnemy.direction == LEFT) {
                newOrigin = [self rotatePoint:bigEnemy.visualAttackBox.position aroundCenter:CGPointMake(bigEnemy.position.x, bigEnemy.size.height/2) withAngularVelocity:bigEnemy.angularVelocityBigEnemyAttack];
            } else {
                newOrigin = [self rotatePoint:bigEnemy.visualAttackBox.position aroundCenter:CGPointMake(bigEnemy.position.x, bigEnemy.size.height/2) withAngularVelocity:-bigEnemy.angularVelocityBigEnemyAttack];
            }
            
            bigEnemy.visualAttackBox.position = newOrigin;
        }
        
        //NSLog(@"new origin: %.1f,%.1f \rAttack box: %.1f,%.1f",newOrigin.x, newOrigin.y, bigEnemy.attackBox.origin.x, bigEnemy.attackBox.origin.y);
    }];
}

//Confere se o ataque do player se chocou com o inimigo
- (void) checkCollisionPlayerAttackBoxWithEnemies{
    
    [_principalLayer enumerateChildNodesWithName:@"enemy" usingBlock:^(SKNode *node, BOOL *stop) {
        SGEnemy *enemy = (SGEnemy *) node;
        
        //Checar se o inimigo ainda esta vivo
        if (enemy.isAlive == YES) {
            
            //Checar se acertou o box do Inimigo
            if(CGRectIntersectsRect(enemy.visualBoundingBox.frame, _player.visualAttackBox.frame)){
                
                //Toca o som de batida no inimigo
                [self runAction:_player.hitEnemySound];
                
                //Inicia particulas de dano
                CGPoint particlesPosition;
                if (_player.direction == RIGHT) {
                    particlesPosition = CGPointMake(_player.visualAttackBox.position.x + _player.visualAttackBox.size.width, _player.visualAttackBox.position.y);
                } else {
                    particlesPosition = CGPointMake(_player.visualAttackBox.position.x - _player.visualAttackBox.size.width, _player.visualAttackBox.position.y);
                }
                
                [enemy initDamageParticlesInPositon:particlesPosition];
                
                //Inicia o Combo se ele não estiver iniciado
                if (!_player.isInCombo) {
                    _player.isInCombo = YES;
                }
                
                //Se ja estiver em combo incrementa o numero de combo
                else {
                    _player.comboNumber += 1;
                    _player.timeSinceLastAttack = 0;
                    
                    //Treme o numero do combo
                    [self shakeComboLabel:8];
                    
                    comboNumLabel.text = [[NSString alloc] initWithFormat:@"%d", _player.comboNumber];
                }
                
                enemy.hp -= 1;//Diminui o HP do inimigo
                
                if(enemy.hp > 0){
                    //Toca o som de sofrimento do inimigo
                    [self runAction:enemy.sufferSound];
                }
                
                [enemy invalidateEnemyRelativoToPlayerDirection:_player.direction];
                
                //Verifica se o inimigo tem HP
                if (enemy.hp <= 0) {
                    [self killEnemy:enemy];
                }
                
                [self littleshakeScreen:5];
            }
        }
    }];
    
    //Verifica a colisao com o big enemy
    [_principalLayer enumerateChildNodesWithName:@"bigEnemy" usingBlock:^(SKNode *node, BOOL *stop) {
        SGEnemy *enemy = (SGEnemy *) node;
        
        //Checar se o inimigo ainda esta vivo
        if (enemy.isAlive == YES) {
            
            //Checar se acertou o box do Inimigo
            if(CGRectIntersectsRect(enemy.visualBoundingBox.frame, _player.visualAttackBox.frame)){
                
                //Toca o som de batida no inimigo
                [self runAction:_player.hitEnemySound];
                
                //Inicia particulas de dano
                CGPoint particlesPosition;
                if (_player.direction == RIGHT) {
                    particlesPosition = CGPointMake(_player.visualAttackBox.position.x + _player.visualAttackBox.size.width, _player.visualAttackBox.position.y);
                } else {
                    particlesPosition = CGPointMake(_player.visualAttackBox.position.x - _player.visualAttackBox.size.width, _player.visualAttackBox.position.y);
                }
                
                [enemy initDamageParticlesInPositon:particlesPosition];
                
                //Inicia o Combo se ele não estiver iniciado
                if (!_player.isInCombo) {
                    _player.isInCombo = YES;
                }
                
                //Se ja estiver em combo incrementa o numero de combo
                else {
                    _player.comboNumber += 1;
                    _player.timeSinceLastAttack = 0;
                    
                    [self shakeComboLabel:8];
                    
                    comboNumLabel.text = [[NSString alloc] initWithFormat:@"%d", _player.comboNumber];
                }
                
                enemy.hp -= 1;//Diminui o HP do inimigo
                
                if(enemy.hp > 0){
                    //Toca o som de sofrimento do inimigo
                    [self runAction:enemy.sufferSound];
                }

                [enemy invalidateEnemyRelativoToPlayerDirection:_player.direction];
                
                //Verifica se o inimigo tem HP
                if (enemy.hp <= 0) {
                    [self killEnemy:enemy];
                }
                
                [self littleshakeScreen:5];
            }
        }
    }];
}

- (void) checkPlayerStatus{
    
    [_player updateVisualBoundingBoxCenter];
    
    if(_player.direction == LEFT && _player.xScale > 0){
        _player.xScale *= -1;
    }
    if(_player.direction == RIGHT && _player.xScale < 0){
        _player.xScale *= -1;
    }
    
}

- (void) checkIfPlayerIsOnFreeFall{
    
    if(_player.velocity.y < 0 && _player.animationStatus == JUMPING){
        //Parar animaçao de pulo
        [_player removeActionForKey:@"playerJumping"];
        
        //Iniciar animaçao de caida
        [_player startFallingAnimation];
    }
}

//Shake de layer WOW \o/
-(void) shakeScreen:(NSInteger)times {
    
    CGPoint initialPos = _shakeLayer.position;
    NSInteger amplitudeX = 32;
    NSInteger amplitudeY = 2;
    NSMutableArray * randomActions = [NSMutableArray array];
    for (int i=0; i<times; i++) {
        NSInteger randX = _shakeLayer.position.x+arc4random() % amplitudeX - amplitudeX/2;
        NSInteger randY = _shakeLayer.position.y+arc4random() % amplitudeY - amplitudeY/2;
        SKAction *action = [SKAction moveTo:CGPointMake(randX, randY) duration:0.01];
        [randomActions addObject:action];
    }
    SKAction *rep = [SKAction sequence:randomActions];
    
    
    [_shakeLayer runAction:rep completion:^{
        _shakeLayer.position = initialPos;
    }];
    
}
-(void) littleshakeScreen:(NSInteger)times {
    
    CGPoint initialPos = _shakeLayer.position;
    NSInteger amplitudeX = 8;
//    NSInteger amplitudeY = 0;
    NSMutableArray * randomActions = [NSMutableArray array];
    for (int i=0; i<times*2; i++) {
        NSInteger randX = _shakeLayer.position.x+arc4random() % amplitudeX - amplitudeX/2;
//        NSInteger randY = _shakeLayer.position.y+arc4random() % amplitudeY - amplitudeY/2;
        SKAction *action = [SKAction moveTo:CGPointMake(randX, 0.0) duration:0.005];
        [randomActions addObject:action];
    }
    SKAction *rep = [SKAction sequence:randomActions];
    
    [_shakeLayer runAction:rep completion:^{
        _shakeLayer.position = initialPos;
    }];
}



//Faz aparecer a  pontuação que com a morte do inimigo
-(void)labelEnemykill: (int)scoredeath withEnemy: (SGEnemy *) enemy{

    NSString *aux2 = [NSString stringWithFormat:@"%i",scoredeath];
    
    //Label que aparece em cima da cabeça do inimigo ao morrer
    SKLabelNode *scoreEnemy = [Text labelNodeWithFontNamed:@"PressStart2P"];
    scoreEnemy.position = CGPointMake(enemy.visualBoundingBox.position.x, enemy.visualBoundingBox.position.y + enemy.visualBoundingBox.size.height/6);
    scoreEnemy.fontSize = 20.0;
    scoreEnemy.text = aux2;
    scoreEnemy.name = @"scoreNumLabel";
    scoreEnemy.fontColor = [SKColor yellowColor];
    [_principalLayer addChild:scoreEnemy];
    
    //Ação de fazer a label desaparecer com uma certa duração de tempo
    [scoreEnemy runAction:[SKAction fadeAlphaTo:0.0 duration:1.0] completion:^{
        [scoreEnemy removeFromParent];
    }];
}

//Aumenta a label de combo
-(void) checkStatusComboLabel{
    
    double numCombo = comboNumLabel.text.intValue;
    comboNumLabel.fontColor = [UIColor whiteColor];
    
    //Limitar o crescimento da fonte
    if(numCombo >= 200.0)
        numCombo = 200.0;
    
    //Funçao de crescimento da fonte
    if(numCombo <= 200 ){
        comboNumLabel.fontSize = 40 + 7*log(numCombo);
    }
    
    //Esconder label quando combo zerado
    if(numCombo == 0){
        comboNumLabel.fontSize = 40;
        comboNumLabel.hidden = YES;
    }
    else
        comboNumLabel.hidden = NO;
}

//Faz o numero de combo tremer quando ele incrementa
-(void)shakeComboLabel:(NSInteger)times{
    CGPoint initialPos = comboNumLabel.position;
    NSInteger amplitudeX = 4 + comboNumLabel.fontSize/7;
    NSInteger amplitudeY = 4 + comboNumLabel.fontSize/7;
    NSMutableArray * randomActions = [NSMutableArray array];
    for (int i=0; i<times; i++) {
        NSInteger randX = comboNumLabel.position.x+arc4random() % amplitudeX - amplitudeX/2;
        NSInteger randY = comboNumLabel.position.y+arc4random() % amplitudeY - amplitudeY/2;
        SKAction *action = [SKAction moveTo:CGPointMake(randX, randY) duration:0.01];
        [randomActions addObject:action];
    }
    SKAction *rep = [SKAction sequence:randomActions];
    
    [comboNumLabel runAction:rep completion:^{
        comboNumLabel.position = initialPos;
    }];
}

//Cria particulas do vento
-(void)createWindParticles{
    _windParticles = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"MyParticle" ofType:@"sks"]];
    
    _windParticles.position = CGPointMake(_background.size.width, 0);
    
    [_principalLayer addChild:_windParticles];
}

@end
