//
//  SGPlayer.h
//  SagaGame
//
//  Created by Lucas Araujo on 4/8/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GlobalVars.h"
#import "SGVector.h"

//Movimentos do player
typedef enum {
    WALKING = 0,
    JUMPING = 1,
    FALLING = 2
    
}PlayerStatus;

@interface SGPlayer : SKSpriteNode

@property (nonatomic) int hp;
@property (nonatomic) CGPoint velocity;
@property (nonatomic) Direction direction;
@property (nonatomic) CGRect boundingBox;
@property (nonatomic) CGRect attackBox;
@property (nonatomic) BOOL isAttacking;
@property (nonatomic) BOOL isInvalid;
@property (nonatomic) BOOL isInCombo;
@property (nonatomic) CGFloat velocityXDefault;
@property (nonatomic) CGFloat velocityXReduced;
@property (nonatomic) BOOL isInvunerable;
@property (nonatomic) SKSpriteNode* visualAttackBox;
@property (nonatomic) int comboNumber;
@property (nonatomic) CGFloat timeSinceLastAttack;
@property (nonatomic) CGFloat lastTimeUpdateCombo;
@property (nonatomic) CGPoint impulse;
@property (nonatomic) BOOL attackingHand;
@property (nonatomic) SKSpriteNode *visualBoundingBox;

//Status do player
@property (nonatomic) PlayerStatus movementStatus;
@property (nonatomic) PlayerStatus animationStatus;

//Actions das animaçoes
@property (nonatomic) SKAction *walkingAnimation;
@property (nonatomic) SKAction *jumpingAnimation;
@property (nonatomic) SKAction *fallingAnimation;
@property (nonatomic) SKAction *hittedAnimation;
@property (nonatomic) SKAction *attackingAnimation;
@property (nonatomic) SKAction *attackingAnimationLeft;
@property (nonatomic) SKAction *attackingAnimationRight;

//Actions de som
@property (nonatomic) SKAction *attackSound;
@property (nonatomic) SKAction *sufferSound;
@property (nonatomic) SKAction *walkSound;
@property (nonatomic) SKAction *dieSound;
@property (nonatomic) SKAction *hitEnemySound;
@property (nonatomic) SKAction *jumpSound;


- (void) invalidatePlayerRelativeToEnemy:(Direction) attackDirection withImpulse:(CGPoint) impulse;
-(void)recoverFromInvalidationInFloorHight: (CGFloat)floor;

- (void) attack;

- (void) jump;

- (void) dash;

- (void) startWalkingAnimation;
- (void) startJumpingAnimation;
- (void) startFallingAnimation;
- (void) startHittedAnimation;
- (void) startAttackingAnimation;

- (void) updateVisualBoundingBoxCenter;

-(void)loadPlayerWalkingAnimations;
-(void)loadPlayerAttackAnimation;
-(void)loadPlayerHittedAnimation;
-(void) loadPlayerJumpingAnimation;
-(void) loadPlayerFallingAnimation;


@end
