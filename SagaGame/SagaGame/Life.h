//
//  Life.h
//  SagaGame
//
//  Created by Tiago Pereira on 4/17/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Life : SKSpriteNode

@property int hpNumber;

@property SKAction *constantAnimation;
@property SKAction *fadingAnimation;

-(void)loadAnimations;

-(void)loadFadingAnimation;
-(void)loadConstantAnimation;

@end
