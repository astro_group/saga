//
//  GameOverScene.m
//  SagaGame
//
//  Created by Naiara Moura on 14/04/15.
//  Copyright (c) 2015 Naiara Moura. All rights reserved.
//

#import "GameOverScene.h"
#import "GameKitHelper.h"
#import "MyScene.h"
#import "Text.h"
#import <Chartboost/Chartboost.h>

@implementation GameOverScene{

    //Varial de maior score
    int maxValue;
    
    //Labels de score
    Text *labelCurrentScore;
    Text *labelNumCurrentScore;
    Text *labelBestScore;
    Text *labelNumBestScore;

}

-(id)initWithSize:(CGSize)size{

    if(self = [super initWithSize:size]){
        
       maxValue = 0;
        self.hasSentScore = NO;
        
        //Backgraund
        self.backgroundColor = [SKColor blackColor];
        
        Text *gameOver = [Text labelNodeWithFontNamed:@"Thirteen Pixel Fonts"];
        gameOver.position = CGPointMake(self.size.width / 2.0, self.size.height-98);
        gameOver.fontSize = 40.0;
        gameOver.text = @"GAME OVER";
        gameOver.name = @"gameOver";
        
        
        //Labels de score
            //Melhor score
        labelBestScore = [Text labelNodeWithFontNamed:@"Thirteen Pixel Fonts"];
        labelBestScore.position = CGPointMake(self.size.width / 2.0, self.size.height-160);
        labelBestScore.fontSize = 25.0;
        labelBestScore.text = @"Best";
        labelBestScore.name = @"labelBestScore";
        
        labelNumBestScore = [Text labelNodeWithFontNamed:@"PressStart2P"];
        labelNumBestScore.position = CGPointMake(self.size.width / 2.0, self.size.height-190);
        labelNumBestScore.fontSize = 20.0;
        labelNumBestScore.text = @" dd";
        labelNumBestScore.name = @"labelNumBestScore";
        
            //Score Atual
        labelCurrentScore = [Text labelNodeWithFontNamed:@"Thirteen Pixel Fonts"];
        labelCurrentScore.position = CGPointMake(self.size.width / 2.0, self.size.height-240);
        labelCurrentScore.fontSize = 25.0;
        labelCurrentScore.text = @"Score";
        labelCurrentScore.name = @"labelCurrentScore";
        
        labelNumCurrentScore = [Text labelNodeWithFontNamed:@"PressStart2P"];
        labelNumCurrentScore.position = CGPointMake(self.size.width / 2.0, self.size.height-270);
        labelNumCurrentScore.fontSize = 20.0;
        labelNumCurrentScore.text = @" dd";
        labelNumCurrentScore.name = @"labelNumCurrentScore";
        

        //Botao de voltar para jogo
        Text *back = [Text labelNodeWithFontNamed:@"Thirteen Pixel Fonts"];
        back.position = CGPointMake(self.size.width -150, self.position.y + 27);
        back.fontSize = 30.0;
        back.text = @"Play";
        back.name = @"back";
        
        //Botao de voltar pro titulo
        Text *title = [Text labelNodeWithFontNamed:@"Thirteen Pixel Fonts"];
        title.position = CGPointMake(150, 27);
        title.fontSize = 30.0;
        title.text = @"Menu";
        title.name = @"titleScreen";
        
      
        [self addChild:gameOver];
        [self addChild:labelBestScore];
        [self addChild:labelNumBestScore];
        [self addChild:labelCurrentScore];
        [self addChild:labelNumCurrentScore];
        [self addChild:back];
        [self addChild:title];
        
        
        //Propaganda Chartboost
        SKAction *wait = [SKAction waitForDuration:0.5];
        SKAction *showAdd = [SKAction runBlock:^{
            [Chartboost showInterstitial:CBLocationHomeScreen];
        }];
        SKAction *showAddSequence = [SKAction sequence:@[wait,showAdd]];
        
        [self runAction:showAddSequence];

    }
    return self;
}

//Ao apertar botao ir para as cenas
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
    SKNode *node = [self nodeAtPoint:location];
    
    SKTransition *transition = [SKTransition fadeWithColor:[UIColor colorWithRed:140.0/255.0
                                                                           green:171.1/255.0
                                                                            blue:186.0/255.0
                                                                           alpha:2]
                                                  duration:2.0];
    

    
    //Se apertar ao botao de back vai para jogo
    if ([node.name isEqualToString:@"back"]) {
               GameScene *game = [[GameScene alloc] initWithSize:CGSizeMake(self.size.width, self.size.height)];
        
        [self.scene.view presentScene:game transition:transition];
        
    }
    
    //Se apertar o butao de quit vai para a tela inicial
    if ([node.name isEqualToString:@"titleScreen"]) {
        MyScene *title = [[MyScene alloc] initWithSize:CGSizeMake(self.size.width, self.size.height)];
        
        [self.scene.view presentScene:title transition:transition];
    }
    

}

//Maior valor que jogador fez
-(void)MaxAndMinPlist{

    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/dadosJogo.plist"];
    NSMutableArray *resultado = [[NSMutableArray alloc]initWithContentsOfFile:path];
    
    for (NSString *strMaxi in resultado) {
        int currentValue =[strMaxi intValue];
        if (currentValue > maxValue) {
            maxValue=currentValue;
        }
    }
   
    labelNumBestScore.text = [NSString stringWithFormat:@"%d",maxValue];
    
}


-(void)update:(NSTimeInterval)currentTime{
    
    //Tranformando valor recebido do score atual em string
    labelNumCurrentScore.text = [NSString stringWithFormat:@"%d", self.valorRecebidoScore];
    
    if(self.hasSentScore == NO){

        [[GameKitHelper sharedGameKitHelper] reportScore:self.valorRecebidoScore forLeaderBoardID:@"Quati.Umbra.leaderboard_score"];
        self.hasSentScore = YES;
    }
    
    //Maior numero da lista
    [self MaxAndMinPlist];
    
    //Caso valor do score atual seja maior que o maior valor label muda de cor
    if (maxValue <= self.valorRecebidoScore) {
        labelCurrentScore.fontColor = [SKColor yellowColor];
        labelNumCurrentScore.fontColor = [SKColor yellowColor];
    }
}


@end
